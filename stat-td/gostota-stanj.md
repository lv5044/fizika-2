Gostota stanj $g(E)$ je porazdelitev. Z njo preko $g(E) \diff E$ preštejemo število stanj, ki imajo energije v intervalu $[E, E + \diff E]$.

Naj bo sistem en delec z valovnim vektorjem $\mathbb k$. Naj ima sistem diskretna energijska stanja. V takem sistemu stanje, ki imajo isto energijo, opisujejo v $\mathbb k$-prostoru površino konstantne energije, določeno preko [disperzijske relacije](https://en.wikipedia.org/wiki/Dispersion_relation) $E = E(\mathbb k)$. Če je disperzijska relacija zvezna, je površina nepretrgana. Več lahko povemo o preprostem, a zelo pogostem primeru, kjer je disperzijska relacije izotropna funkcija

$$
E = E(k), \qquad k = \| \mathbb k \|.
$$

Za izbrano energijo $E$ dobimo v $\mathbb k$-prostoru sfero z radijem $k$, ki ga lahko izračunamo iz disperzijske relacije. Če je disperzijska relacija (strogo) naraščajoča funkcija, je radij sfere enolično določen in sfera omejuje kroglo vseh stanj z energijo manjšo od $E$. Število stanj v tej krogli je sorazmerno z volumnom izseka krogle

$$
\Omega(k) = \frac{\beta_n}{2^n} k^n,
$$  

pri čemer je $\beta_n$ geometrijski faktor, volumen enotske $n$-krogle. Faktor $2^n$ odraža, da so komponente $\mathbb k$, ki opisujejo površino krogle, lahko le pozitivne. Za vsako od $n$ koordinat to prostor razdeli na dve polovici.

| $n$ | $\beta_n/2^n$      |
| :-: | --:                |
|  1  | 1                  |
|  2  | $\pi/4$            |
|  3  | $\frac{\pi}{6}$    |
|  4  | $\frac{\pi^2}{32}$ |

To primerjamo z volumnom osnovne enote faznega prostora, t. j. volumnom, v katerega umestimo eno stanje.  Ta volumen je določen z energijskim lastnim stanjem, ki preko inverzne disperzijske relacije pripada najmanjšemu valovnemu številu $k_0$. Če je $E(k)$ (strogo) naraščajoča, je to osnovno stanje sistema z energijo $E_0 = E(k_0)$.

$$
\Omega_0 = k_0^n = \left( \frac{2\pi}{L} \right)^n.
$$

Število stanj (brez upoštevanja degeneracij), ki imajo energijo manjšo od $E$, je 

$$
N(k) = \left( \frac{L}{2\pi} \right)^n \beta_n k^n.
$$

Število stanj z energijo v $[E, E + \diff E]$ je preprosto

$$
\diff N = \frac{\diff N}{\diff E} \diff E,
$$

torej je gostota stanj

$$
g(E) = \frac{\diff N}{\diff k} \frac{\diff k}{\diff E}.
$$

**Elektronski plin.** Disperzijska relacija elektrona v neskončni potencialni jami je

$$
E(k) = E_0 + \frac{\hbar^2 k^2}{2m}.
$$

Za izbrano energijo $E$ lahko valovno število izrazimo kot

$$
k(E) = \left( \frac{2m(E - E_0)}{\hbar^2} \right)^{\frac{1}{2}}
$$

Za $n = 3$, primer 3-dimenzionalnega plina, je stanj z energijo v $[E, E + \diff E]$

$$
N(E) = s \left( \frac{L}{2\pi} \right)^3 \frac{\pi}{6} \left( \frac{2m(E - E_0)}{\hbar^2} \right)^{\frac{3}{2}},
$$

kjer smo dodali faktor $s$, ki označuje degeneracijo, recimo zaradi spina. V našem primeru so stanja degenerirana glede na spin, torej je $s = 2$. Odvod števila po energiji je gostota stanj

\begin{align*}
g(E) &= \frac{\pi}{2} \left( \frac{L}{2\pi} \right)^3 \left( \frac{2m(E - E_0)}{\hbar^2} \right)^{3/2} \\
     &= \frac{4\pi V (2m)^{3/2}}{\hbar^3}  \sqrt{E - E_0}.
     &= \frac{V (2m)^{3/2}}{2\pi^2 h^3}  \sqrt{E - E_0}.
\end{align*}
