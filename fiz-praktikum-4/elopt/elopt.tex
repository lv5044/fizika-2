% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

\title{Elektrooptični pojav}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Električno polje vpliva na mnogo lastnosti snovi. Pri eksperimentu opazujemo, kako v homogeni keramiki statična polja (polja s frekvenco bistveno nižjo od frekvence svetlobe) vplivajo na optične lastnosti snovi. Zunanje polje zmoti izotropnosti -- dobimo različna lomna količnika -- eno za svetlobo, polarizirano vzporedno z zunanjim poljem in eno za svetlobo s prevokotno polarizacijo. To imenujemo kvadratni elektroptični pojav, ker je pri pojavu razlika lomnih količnikov enaka

\begin{equation}
    n_\parallel - n_\perp = B\lambda E^2.
    \label{eq:n-diff}
\end{equation}

Pri tem je $\lambda$ valovna dolžina svetlobe, $E$ jakost električnega polja ter $B$ t. i. Kerrova konstanta.

Na optični mizi imamo dva polarizatorja, zamaknjena za $\SI{90}{\degree}$. Med njima imamo postavljeno Kerrovo celico -- kos elektroptičnega materiala med elektrodama, ki zagotavljata polje $\mathbf E$. Polje je ravnini, pravokotni na optično os, pod kotom $\SI{45}{\degree}$ glede na prvi polarizator. Če celico odstranimo, bo jakost svetlobe za drugim polarizatorjem preprosto 0. Podobno je jakost za drugim polarizatorjem 0, če je celica prisotna, a je polje $\mathbf E = 0$.

Zdaj celico priklopimo na napetost. Polje $\mathbf E$ je zdaj različno od 0. To napravi celico dvolomno. Po načelu superpozicije obravnavamo komponenti valovanja v dveh pravokotnih smereh, ki jim pripadata lomna količnika $n_\parallel, n_\perp$. V Kerrovo celico vstopi svetloba (svetloba je E. M. valovanje in gledamo le električno polje)

\begin{equation*}
    \ket{\mathcal{E}_\uparrow} = \mathcal{E}_0\ket{\uparrow},
\end{equation*}

pri čemer je $\mathcal{E}_0$ amplituda električnega polja svetlobe. Ker velja $\sqrt 2\ket{\mathcal{E}_\parallel} = \ket{\mathcal{E}_\uparrow} + \ket{\mathcal{E}_\rightarrow}$ in $\sqrt 2\ket{\mathcal{E}_\perp} = \ket{\mathcal{E}_\uparrow} - \ket{\mathcal{E}_\rightarrow}$ pride iz celice svetloba

\begin{equation*}
    \ket{\mathcal{E}} = \frac{e^{ik_\parallel L}}{\sqrt 2} \ket{\mathcal{E}_\parallel} + \frac{e^{ik_\perp L}}{\sqrt 2} \ket{\mathcal{E}_\perp}.
\end{equation*}

Zdaj uporabimo $k_\parallel - k_\perp = k(n_\parallel - n_\perp)$, kjer razliko lomnih količnikov poznamo iz~(\ref{eq:n-diff}). Vemo tudi, da je $k = 2\pi/\lambda$, torej je svetloba za drugim polarizatorjem

\begin{align*}
    \braket{\uparrow}{\mathcal{E}} &= \frac{e^{ik_\parallel L}}{2} \braket{\uparrow}{\mathcal{E}_\uparrow + \mathcal{E}_\rightarrow} + \frac{e^{ik_\perp L}}{2} \braket{\uparrow}{-\mathcal{E}_\uparrow + \mathcal{E}_\rightarrow} \\
                                           &= \frac{e^{ik_\perp L}}{2} \left( e^{i2\pi BE^2L} - 1 \right) \mathcal{E}_0 \\
\end{align*}

jakost svetlobe $j$, s tem pa tudi tok $I$, zaznan na fotodiodi, sta sorazmeren s kvadratom absolutne vrednosti električnega polja svetlobe

\begin{align*}
    I &\propto \abs{\braket{\uparrow}{\mathcal{E}_\uparrow}}^2 \\
      &\propto \frac{1}{4} \abs{e^{i2\pi BE^2L} - 1}^2 \\
      &= \frac{1}{4} \left( 2 - 2\cos (2\pi BE^2L) \right) \\
      &= \sin^2(\pi BE^2L).
\end{align*}

To velja v idealizaciji, kjer se celica pri polju $\mathbf E = 0$ res zgolj asorbira del svetlobe. Zares pa je tudi pri odsotnosti polja celica nekoliko dvolomna, torej moramo fazni razliki $2\pi BE^2L$ prišteti neko konstanto $\delta$. Če upoštevamo še, da je jakost polja $E = U/d$, je tok na fotodiodi oblike

\begin{equation}
    I = I_1 \sin^2 \left( \frac{\pi BLU^2}{d^2} + \frac{\delta}{2} \right)
    \label{eq:I-Kerr}
\end{equation}

\section{Naloga}

\begin{enumerate}
    \item Izmerite kotno odvisnost prepustnosti polarizatorja za linearno polarizirano svetlobo.
    \item Izmerite prepustnost dveh pravokotno postavljenih polarizatorjev, ko mednju postavite še tretji polarizator in ga vrtite.
    \item Določite Kerrovo konstanto PLZT keramike.
\end{enumerate}

\section{Meritve in račun}

\subsection{Prvi polarizator}

Na optični mizi imamo že navpično polariziran laser in en polarizator. Laser poravnamo, da detektor zadane v sredino. Polarizator vrtimo in najdemo kot\footnote{Kot merimo od y-osi polarizatorja, v pozitivno smer, če žarek kaže iz strani.} z minimalno jakostjo prepuščene svetlobe, torej najmanjšim tokom na fotodiodi. Nato vrtimo polarizator po korakih $\SI{5}{\degree}$ in na računalniku beležimo tok na fotodiodi (slika~\ref{fig:I-by-angle}, zgoraj). Skozi meritve prilagodimo funkcijo oblike

\begin{figure}
    \centering
    \includegraphics{I-by-angle.pdf}
    \caption{Odvisnost energijskega toka (oz. izmerjenega električna toka skozi fotodiodo) od kota enega, nato pa drugega dodatnega polarizatorja in skozi meritve prilagojene krivulje.}
    \label{fig:I-by-angle}
\end{figure}

\begin{equation*}
    I = I_{1\alpha} \cos^2(\alpha - \delta_\alpha),
\end{equation*}

in dobimo parametre

\begin{align*}
    I_{1\alpha} &= (320 \pm 2)\,\si{\mu A}, \\
    I_{0\alpha} &= (-2 \pm 1)\,\si{\mu A}, \\
    \delta_\alpha &= (-5.7 \pm 0.2)\,\si{\degree}.
\end{align*}

Fazni zamik $\delta_\alpha$ si razložimo kot kot med y-osjo polarizatorja in polarizacijsko osjo laserja.

\subsection{Drugi polarizator}

Prvi polarizator nastavimo nazaj na kot z minimalno jakostjo prepuščene svetlobe. Med njim in že navpično polariziranim laserjem postavimo drugi polarizator. Če je vmesni polarizator obrnjen navpično ali pravokotno, je prepustnost minimalna, za kotih vmes pa oba polarizatorja skupaj prepuščata del svetlobe. Spet najdemo kot z minimalno prepustnostjo in od tam nadaljujemo meritev (slika~\ref{fig:I-by-angle}, spodaj).

Skozi meritve prilagodimo funkcijo oblike

\begin{equation*}
    I = I_{1\alpha} \sin^2(\alpha - \delta_\alpha),
\end{equation*}

in dobimo parametre

\begin{align*}
    I_{1\beta} &= (56.4 \pm 0.7)\,\si{\mu A}, \\
    I_{0\beta} &= (1.0 \pm 0.4)\,\si{\mu A}, \\
    \delta_\beta &= (-14.3 \pm 0.4)\,\si{\degree}.
\end{align*}

V začetni postavitvi prvi polarizator postavljen pravokotno na polarizacijsko os laserja. Fazni zamik $\delta_\alpha$ si torej spet razložimo kot kot med y-osjo polarizatorja in polarizacijsko osjo laserja.

\subsection{Kerrova celica}

Odstranimo drugi polarizator in prvega nastavimo nazaj na kot z minimalno jakostjo prepuščene svetlobe. Med laser in polarizator postavimo Kerrovo celico. Vklopimo visokonapetostni izvir in pomerimo celotno napetostno območje (slika~\ref{fig:I-by-U}).

\begin{figure}
    \centering
    \includegraphics{I-by-U.pdf}
    \caption{Odvisnost energijskega toka (oz. izmerjenega električna toka skozi fotodiodo) od napetosti v Kerrovi celici. Skozi meritve sta prilagojeni dve funkciji, črtkana ne upošteva prvih 5 meritev.}
    \label{fig:I-by-U}
\end{figure}

Skozi meritve prilagodimo funkcijo oblike~(\ref{eq:I-Kerr}). Če pri tem uporabimo vse meritve, dobimo

\begin{align*}
    I_1 &= (91.6 \pm 1.5)\,\si{\mu A}, \\
    B &= (1.17 \pm 0.04)\,\si{\mu A}, \\
    \delta &= (-44 \pm 4)\,\si{\degree}.
\end{align*}

Vidimo, da je prileganje pri nižjih napetostih slabše. Če namreč funkcijo prilagodimo in pri tem ne upoštevamo prvih 5 meritev, je ujemanje nekoliko boljše

\begin{align*}
    I_1 &= (91.3 \pm 0.6)\,\si{\mu A}, \\
    B &= (1.29 \pm 0.02)\,\si{\mu A}, \\
    \delta &= (-60 \pm 3)\,\si{\degree}.
\end{align*}

Preko tega lahko sumimo, da je model pri nizkih napetostih nekoliko pomankljiv in ne opiše realnega delovanja naše eksperimentalne postavitve.

\end{document}
