% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}
\usepackage{physics}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{subfig}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage[svgnames]{xcolor}

% Pgfplots
\usepackage{amssymb}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

% Styling
\numberwithin{equation}{section}
\setlength{\skip\footins}{1.5cm}

\title{Sklopljena nihajna kroga}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Mnogo pojavov v naravi lahko opišemo kot sistem skopljenih enakih oscilatorjav. Sistem, sestavljen s sklopitvijo $n$ ima $n$ lastnih načinov nihanja z lastnimi frekvencami $\omega_n$. V primeru sklopljenih fizičnih nihal imamo prvi način, kjer nihali nihata v fazi, ter drugi način, kjer nihata v protifazi. V prvem načinu je lastna frekvenca enaka lastni frekvenci enega nesklopljenea nihala. V drugi je večja -- tem bolj, čim močnejša je sklopitev.

Rešitev diferencialna enačbe za zgolj kapacitivno sklopljena kroga~\ref{fig:sketch} nam za začetni pogoj, kjer drugi krog miruje in začnemo vzbujati prvi krog, napove odvisnost napetosti oblike

\begin{align*}
    U_1 &= U_0 e^{-\beta t} \cos(\omega t) \cos(\Delta\omega t), \\
    U_2 &= U_0 e^{-\beta t} \cos(\omega t) \sin(\Delta\omega t),
\end{align*}

ki jo vidimo tudi na sliki~\ref{fig:model}.

\begin{figure}
    \centering
    \subfloat{%
            \begin{tikzpicture}
                \begin{axis}[width=0.45\textwidth,
                    xmin=0, xmax=2, ymin=-1.1, ymax=1.1,
                    xlabel={$\Delta\omega t / 2\pi$}, ylabel={$U_1 / U_0$},
                    xtick={1, 2}]
                    \addplot[domain=0:2,
                        samples=500,
                        smooth, thick, SeaGreen]
                        {exp(-1.2*x) * cos(deg(2*pi*x)) * cos(deg(16*2*pi*x))};
                    \addplot[domain=0:2,
                        samples=500,
                        smooth, thin, DarkSlateGrey]
                        {exp(-1.2*x) * cos(deg(2*pi*x))};
                    \addplot[domain=0:2,
                        samples=500,
                        dashed, thick, DarkSlateGrey]
                        {exp(-1.2*x)};
                \end{axis}
    \end{tikzpicture}}
    \qquad
    \subfloat{%
            \begin{tikzpicture}
                \begin{axis}[width=0.45\textwidth,
                    xmin=0, xmax=2, ymin=-1.1, ymax=1.1,
                    xlabel={$\Delta\omega t / 2\pi$}, ylabel={$U_2 / U_0$},
                    xtick={1, 2}]
                    \addplot[domain=0:2,
                        samples=500,
                        smooth, thick, Salmon]
                        {exp(-1.2*x) * sin(deg(2*pi*x)) * cos(deg(16*2*pi*x))};
                    \addplot[domain=0:2,
                        samples=500,
                        smooth, thin, DarkSlateGrey]
                        {exp(-1.2*x) * sin(deg(2*pi*x))};
                    \addplot[domain=0:2,
                        samples=500,
                        dashed, thick, DarkSlateGrey]
                        {exp(-1.2*x)};
                \end{axis}
    \end{tikzpicture}}
    \caption{Modelski napetosti na prvem in drugem krogu.}
    \label{fig:model}
\end{figure}

Naša eksperimentalna postavitev pa ni ravno tako idealna -- povsem je problem induktivna sklopitev, ki pri manjših vrednostih nastavljivega kondenzatorja igra precej veliko vlogo, a smo jo tu ignorirali.

Ko merimo resonančno krivuljo, vidimo, da je resonančni vrh položnejši, čim večje je dušenje $\beta$. Pogosto namesto parametra $\beta$ navajamo dobroto oz. kvaliteto nihajnega kroga

\begin{equation}
    Q = \frac{\omega_1}{\Delta\omega} = \frac{\omega}{2\beta} = \sqrt{\frac{L}{CR^2}},
    \label{eq:Q}
\end{equation}

pri čemer je $\omega_1$ resonančna frekvanca, $\Delta\omega$ širina širina resonančne krivulje pri $\frac{1}{\sqrt 2}$ maksimuma.

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{circuit-diagram.png}
    \caption{Sklopljena nihajna kroga, napajana s tokovnim izvorom $I_1$. Sklopitev je izvedena s kondenzatorjem $C_0$, ki ima spremenljivo kapaciteto ($R = \SI{7.5}{\ohm}$, $R_g = \SI{220 }{k\ohm}$, $C = \SI{5.6}{nF}$).}
    \label{fig:sketch}
\end{figure}

\section{Naloga}

\begin{enumerate}
    \item Izmerite časovni potek napetosti na obeh krogih pri vzbujanju s stopničastim signalom za vse različne sklopitve $C_0 = 0, 150, 330, 560, 820, 1150\,\si{pF}$.
    \item Izmerite frekvenčno karakteristiko enega nihajnega kroga in določite dobroto $Q$.
    \item Izmerite frekvenčno karakteristiko sklopljenih nihajnih krogov z meritvijo odziva drugega kroga za vsak $C_0$ in izmerite razliko lastnih krožnih frekvenc $\Delta\omega$
\end{enumerate}

\section{Meritve in račun}

V prvem delu naloge pri različnih kapacitivnih sklopitvah posnamemo potek napetosti $U_1$ v prvem krogu, ki ga napajamo direktno in napetosti $U_2$ v krogu, ki je vzbujan preko kapacitivne sklopitve. Poteke vidimo na slikah~\ref{fig:U-by-t-150},~\ref{fig:U-by-t-330},~\ref{fig:U-by-t-560},~\ref{fig:U-by-t-820} in~\ref{fig:U-by-t-1150}.

Prilagajanje modelske krivulje zaradi neprimernosti modela ne pride v poštev. Zato iz grafov razberemo frekvence $\omega$ (tabeli~\ref{tab:small-1}, \ref{tab:small-2}) in parametra ovojnice (tabeli~\ref{tab:large-1}, \ref{tab:large-2}).

\begin{table}
    \centering
    \begin{tabular}{c|r r r}
        $C\,[\si{pF}]$ & $N$ & $N t_0\,[\si{\mu s}]$ & $\omega\,[\si{\mu s^{-1}}]$ \\
        \hline
        150 & 15 & $230 \pm 5$ & $0.410 \pm 0.009$ \\
        330 & 37 & $570 \pm 5$ & $0.408 \pm 0.004$ \\
        560 & 36 & $580 \pm 5$ & $0.390 \pm 0.003$ \\
        820 & 31 & $530 \pm 5$ & $0.380 \pm 0.004$ \\
        1150 & 33 & $540 \pm 5$ & $0.384 \pm 0.004$
    \end{tabular}
    \caption{Frekvenca napetosti $U_1$.}
    \label{tab:small-1}
    \bigskip
    \begin{tabular}{c|r r r}
        $C\,[\si{pF}]$ & $N$ & $N t_0\,[\si{\mu s}]$ & $\omega\,[\si{\mu s^{-1}}]$ \\
        \hline
        150 & 16 & $240 \pm 5$ & $0.419 \pm 0.009$ \\
        330 & 30 & $440 \pm 5$ & $0.428 \pm 0.005$ \\
        560 & 30 & $460 \pm 5$ & $0.410 \pm 0.004$ \\
        820 & 30 & $460 \pm 5$ & $0.410 \pm 0.004$ \\
        1150 & 30 & $460 \pm 5$ & $0.410 \pm 0.004$ \\
    \end{tabular}
    \caption{Frekvenca napetosti $U_2$.}
    \label{tab:small-2}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{c|r r|r r r}
        $C\,[\si{pF}]$ & $T_0\,[\si{\mu s}]$ & $\Delta\omega\,[\si{ms^{-1}}]$ & $U(0)\,[\si{mV}]$ & $U(T_0)\,[\si{mV}]$ & $\beta\,[\si{ms^{-1}}]$ \\
        \hline
        150 & $(230 \pm 5)$ & & $8.5 \pm 0.2$ & $1.0 \pm 0.2$ &  $9.3 \pm 0.9$ \\
        330 & $250 \pm 5$ & $25.1 \pm 0.5$ & $8.4 \pm 0.2$ & $1.2 \pm 0.2$ & $7.8 \pm 0.7$ \\
        560 & $155 \pm 3$ & $40.5 \pm 0.8$ & $8.0 \pm 0.2$ & $2.8 \pm 0.2$ & $6.8 \pm 0.5$ \\
        820 & $116 \pm 1$ & $54.5 \pm 0.5$ & $8.2 \pm 0.2$ & $4.2 \pm 0.2$ & $5.8 \pm 0.5$ \\
        1150 & $93 \pm 1$ & $67.6 \pm 0.7$ & $7.6 \pm 0.2$ & $4.6 \pm 0.2$ & $5.4 \pm 0.6$
    \end{tabular}
    \caption{Frekvenca ovojnice in parameter $\beta$ ovojnice poteka napetosti $U_1$.}
    \label{tab:large-1}
    \bigskip
    \begin{tabular}{c|r r|r r r}
        $C\,[\si{pF}]$ & $T_0\,[\si{\mu s}]$ & $\Delta\omega\,[\si{ms^{-1}}]$ & $U(0)\,[\si{mV}]$ & $U(T_0)\,[\si{mV}]$ & $\beta\,[\si{ms^{-1}}]$ \\
        \hline
        150 & $120 \pm 20$ & $26 \pm 2$ & & & \\
        330 & $330 \pm 10$ & $20 \pm 1$ & $4.2 \pm 0.1$ & $0.8 \pm 0.1$ & $5.3 \pm 0.5$ \\
        560 & $175 \pm 5$ & $36 \pm 1$ & $5.4 \pm 0.2$ & $2.0 \pm 0.2$ & $6 \pm 1$ \\
        820 & $120 \pm 2$ & $55 \pm 1$ & $6.0 \pm 0.1$ & $3.0 \pm 0.2$ & $5.8 \pm 0.6$ \\
        1150 & $95 \pm 2$ & $66 \pm 1$ & $6.4 \pm 0.2$ & $3.8 \pm 0.2$ & $5.5 \pm 0.7$
    \end{tabular}
    \caption{Frekvenca ovojnice in parameter $\beta$ ovojnice poteka napetosti $U_2$.}
    \label{tab:large-2}
\end{table}

Nato kratko sklopljena kroga priključimo na merilnik napetosti, ki ga nadzorujemo z računalniškim programom in prečešemo frekvenčno območje od $\SI{45}{kHz}$ do $\SI{80}{kHz}$ (to pomeni krožne frekvence od $\SI{0.28}{\mu s^{-1}}$ do $\SI{0.50}{\mu s^{-1}}$). Resonančno krivuljo posnamemo tudi za kroga, ki nista v stiku (slika~\ref{fig:res-no-cap}) -- tu opazimo dva resonančna vrhova, ki ju povzroči induktivna sklopitev. Za resonančni vrh, ki ga najdemo pri

\begin{equation*}
    \omega_1 = (0.425 \pm 0.002)\,\si{\mu s^{-1}},
\end{equation*}

izračunamo širino pri $\frac{1}{\sqrt 2}$ maksimuma

\begin{equation*}
    \Delta\omega = (0.013 \pm 0.002)\,\si{\mu s^{-1}}.
\end{equation*}

Iz tega lahko po enačbi~(\ref{eq:Q}) izračunamo dobroto

\begin{equation*}
    Q = 33.9 \pm 0.2,
\end{equation*}

ter parameter dušenja

\begin{equation*}
    \beta = (6 \pm 1)\,\si{ms^{-1}}.
\end{equation*}

Enako meritev resonanc ponovimo še za vse kapacitivne sklopitve nihajnih krogov. Resonančne krivulje narišemo skupaj na sliki~\ref{fig:res}. Vsi maksimumi so navedeni v tabeli~\ref{tab:peaks}.

V resonancah pri največji možni sklopitvi, torej pri $C_0 = \SI{1150}{pF}$, priklopimo nazaj osciloskop, da napetosti $U_1$, $U_2$ prikažemo v \verb|XY| načinu. Iz doblje Lissajoujeve elipse lahko fazni zamik $\delta = \operatorname{arg}(U_2/U_1)$ izračunamo kot

\begin{equation*}
    \sin\delta = \frac{U_2(U_1 = 0)}{U_{2,\mathrm{max}}}.
\end{equation*}

Na ta način ocenimo, da je v prvem maksimumu pri $\omega_1 = (0.353 \pm 0.02)\,\si{\mu s^{-1}}$ fazni zamik

\begin{equation*}
    \delta_1 = (180 \pm 10)\,\si{\degree},
\end{equation*}

pri drugem maksimumu pri $\omega_2 = (0.435 \pm 0.02)\,\si{\mu s^{-1}}$ pa

\begin{equation*}
    \delta_2 = (3 \pm 10)\,\si{\degree}.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{res-no-cap.pdf}
    \caption{Resonančne krivulje brez sklopitve .}
    \label{fig:res-no-cap}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{res.pdf}
    \caption{Resonančne krivulje pri različnih načinih (kapacitivne) sklopitve.}
    \label{fig:res}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{c|r r}
        $C\,[\si{pF}]$ & $\omega_1\,[\si{\mu s^{-1}}]$ & $\omega_2\,[\si{\mu s^{-1}}]$ \\
        \hline
        kratki stik & $0.425 \pm 0.002$ & \\
        nepovezana & $0.423 \pm 0.002$ & $0.437 \pm 0.002$  \\
        $150$ & $0.415 \pm 0.002$ & $0.437 \pm 0.002$ \\
        $330$ & $0.396 \pm 0.002$ & $0.437 \pm 0.002$ \\
        $560$ & $0.382 \pm 0.002$ & $0.437 \pm 0.002$ \\
        $820$ & $0.366 \pm 0.002$ & $0.437 \pm 0.002$ \\
        $1150$ & $0.353 \pm 0.002$ & $0.435 \pm 0.002$
    \end{tabular}
    \caption{Resonance za $U_1$ pri različnih sklopitvah.}
    \label{tab:peaks}
\end{table}

\begin{figure}
    \centering
    \includegraphics{U-by-t-150.pdf}
    \caption{Poteki napetosti v prvem in drugem krogu pri $C_0 = \SI{150}{pF}$.}
    \label{fig:U-by-t-150}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{U-by-t-330.pdf}
    \caption{Poteki napetosti v prvem in drugem krogu pri $C_0 = \SI{330}{pF}$.}
    \label{fig:U-by-t-330}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{U-by-t-560.pdf}
    \caption{Poteki napetosti v prvem in drugem krogu pri $C_0 = \SI{560}{pF}$.}
    \label{fig:U-by-t-560}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{U-by-t-820.pdf}
    \caption{Poteki napetosti v prvem in drugem krogu pri $C_0 = \SI{820}{pF}$.}
    \label{fig:U-by-t-820}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{U-by-t-1150.pdf}
    \caption{Poteki napetosti v prvem in drugem krogu pri $C_0 = \SI{1150}{pF}$.}
    \label{fig:U-by-t-1150}
\end{figure}

\end{document}
