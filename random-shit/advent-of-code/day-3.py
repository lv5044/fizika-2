import requests
import numpy as np


# Get the input file
day = 3
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
bits = np.array(
    [list(map(int, list(line))) for line in input.splitlines()])
col_mean = np.mean(bits, axis=0)
gamma = ''.join([str(1 if m >= 0.5 else 0) for m in col_mean])
epsilon = ''.join([str(0 if m >= 0.5 else 1) for m in col_mean])
print(int(gamma, 2) * int(epsilon, 2))

def rating(type, bits):
    ''' Calculates a rating of given type. Type is assumed to be 'g' for
    generator or 's' for scrubber.'''
    m, n = bits.shape
    i, indicies = 0, np.array([True]*m)
    while i < n and np.count_nonzero(indicies) > 1:
        col = bits[:,i]
        m = np.mean(col[indicies])
        if type == 'g':
            most_common = 1 if m >= 0.5 else 0
        else:
            most_common = 0 if m >= 0.5 else 1
        indicies *= (col == most_common)
        i += 1
    remaining = bits[np.argwhere(indicies)[0,0],:]
    return int(''.join(map(str, remaining)), 2)

print(rating('g', bits) * rating('s', bits))
