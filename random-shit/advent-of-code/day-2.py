import requests
import numpy as np


# Get the input file
day = 2
url = f'https://adventofcode.com/2021/day/{day}/input'
cookies = {'session': '53616c7465645f5f0e4afb83d9f2da99f9ce44045e47abd49e8718357fc0a02ca94a58229b88aeed212daaa4f2cefc9e'}
r = requests.get(url, cookies=cookies)
input = r.content.decode('utf-8')

# Da code
commands = [line for line in input.splitlines()]

pos = np.array([0, 0])
for cmd in commands:
    direction, X = cmd.split()
    base = {
        'forward': [1, 0],
        'down': [0, 1],
        'up': [0, -1]
    }
    pos += int(X) * np.array(base[direction])
x, h = pos
print(x*h)

pos = np.array([0, 0, 0])
for cmd in commands:
    direction, X = cmd.split()
    _, _, aim = pos
    base = {
        'down': [0, 0, 1],
        'up': [0, 0, -1],
        'forward': [1, aim, 0],
    }
    pos += int(X) * np.array(base[direction])
x, h, _ = pos
print(x*h)
