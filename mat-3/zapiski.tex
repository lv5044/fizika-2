% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[b5paper,margin=3cm]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Packages
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{harpoon}

% Styling
\numberwithin{equation}{subsection}
\setlength{\skip\footins}{1.5cm}

% Scientific notation
\newcommand{\scinot}[2]{\num[round-precision=#1,round-mode=figures,exponent-product=\ensuremath{\cdot}]{#2}}

% Vector
\renewcommand*{\vec}[1]{\overrightharp{\ensuremath{#1}}}

\renewcommand{\theequation}{\thesubsection\alph{equation}}

% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}

% Section and subsection styles
\makeatletter

\renewcommand\section{\@startsection{section}{1}{\z@}
    {3.5ex \@plus -1ex \@minus -.2ex}
    {4ex \@plus .2ex}
    {\normalfont\bfseries\MakeUppercase}}

\renewcommand\subsection{\@startsection{subsection}{1}{\z@}
    {3.5ex \@plus -1ex \@minus -.2ex}
    {-1.5ex \@plus .2ex}
    {\normalfont\bfseries}}

\makeatother

\title{Zapiski pri matematiki 3}
\author{Martin Šifrar}

\begin{document}

\maketitle

\newpage

\section{Integrali z parametrom}

\subsection{Definicija} Integral s parametrom je funkcija $F$, podana kot

\begin{equation}
    F(t) \das \int_a^b f(x, t) \,\diff x,
\end{equation}

za neko funkcijo $f$ in $a, b \in \mathbb R$.

\subsection{Trditev} \label{sec:zveznost-1} Naj bodo $a,b,c,d \in \mathbb R$ in naj bo $f: [a,b] \times [c,d]$ zvezna. Tedaj je njen integral s parametrom

\begin{equation*}
    F(t) \das \int_a^b f(x, t) \,\diff x
\end{equation*}

spet zvezna funkcija.

\subsection*{Dokaz} Ker je $f$ zvezna, je za poljuben $t$ tudi $x \to f(x,t)$ zvezna na $[a,b]$. Za poljubni točki $t,t_0 \in [c,d]$ lahko ocenimo

\begin{equation} \label{eq:razlika-integralov}
    |F(t) - F(t_0)| = \left| \int_a^b f(x,t) - f(x,t_0) \,\diff x \right| \le \int_a^b |f(x,t) - f(x,t_0)| \,\diff x.
\end{equation}

Ker je $f$ zvezna na zaprtem kvadrata $[a,b] \times [c,d]$, je tudi enakomerno zvezna. Za poljubno majhen $\varepsilon > 0$ torej lahko najdemo tak $\delta > 0$, da je

\begin{equation*}
    |t - t_0| < \delta \implies |f(x,t) − f(x,t_0)| < \frac{\varepsilon}{b - a} \textrm{ za vsak } x \in [a,b].
\end{equation*}

S tem lahko dodatno navzgor ocenimo \ref{eq:razlika-integralov}

\begin{equation*}
    |F(t) − F(t_0)| < \int_a^b \varepsilon \,\diff x = \varepsilon.
\end{equation*}

$\hfill\blacksquare$

\subsection*{Oznaka} Od zdaj naprej $[a,b] \times [c,d]$ krajše označujemo $\mathcal P$.

\subsection{Trditev} \label{sec:odvod-skozi-integral} Naj bo funkcija $f: \mathcal P \to \mathbb R$ zvezno odvedljiva. Tedaj je njen integral s parametrom odvedljiva funkcija na $[c,d]$ in velja

\begin{equation*}
    F'(t) = \frac{\partial}{\partial t} \int_a^b f(x,t) \,\diff x = \int_a^b \frac{\partial f}{\partial t} (x,t) \,\diff x.
\end{equation*}

\subsection*{Dokaz} Odvod gledamo v poljubni točki $t_0 \in (c,d)$. Za vsak $h \in \{h \in \mathbb R : t_0 + h \in [c,d]\}$ je

\begin{equation} \label{eq:ne-čisto-odvod}
    \frac{F(t_0 + h) - F(t_0))}{h} = \int_a^b \frac{f(t_0 + h) - f(t_0)}{h} \,\diff x.
\end{equation}

Po Lagrangevem izreku obstaja nek $\vartheta \in (0,h)$, tako da v točki $t_0 + \vartheta$ velja

\begin{equation*}
    \frac{\partial f}{\partial t} (x,t_0 + \vartheta) = \frac{f(t_0 + h) - f(t_0)}{h}.
\end{equation*}

Ker je funkcija $\frac{\partial f}{\partial t} (x,t)$ zvezna na zaprtem kvadratu $\mathcal P$, je enakomerno zvezna. Za poljubni točki $u,v \in \mathcal P$ velja

\begin{equation*}
    \exists \delta > 0: |u - v| < \delta \implies \left| \frac{\partial f}{\partial t} (u) - \frac{\partial f}{\partial t} (v) \right| < \frac{\varepsilon}{b - a},
\end{equation*}

kjer je $\varepsilon$ poljubno majhen. Poleg tega sta točki $(x,t_0 - \vartheta)$ in $(x,t_0)$ oddaljeni za $|\vartheta| < |h|$. Odvod $F'(t)$, ki nas končno zanima, je limita izraza~\ref{eq:ne-čisto-odvod} v $h \to 0$, torej imamo zagotovljene poljubno majhne vrednosti $\delta$, s tem pa torej omejen izraz

\begin{align*}
    \frac{F(t_0 + h) - F(t_0)}{h} &- \int_a^b \frac{\partial f}{\partial t} (x,t_0) \,\diff x \\
    = \int_a^b \frac{\partial f}{\partial t} (x,t_0 + \vartheta) \,\diff x &< \int_a^b \frac{\varepsilon}{b - a} \,\diff x = \varepsilon.
\end{align*}

$\hfill\blacksquare$

\subsection{Posledica} \label{sec:verižno} Naj bo funkcija $f: \mathcal P \to \mathbb R$ zvezno odvedljiva. Tedaj je 

\begin{equation*}
    F(t) \das \int_{\alpha(t)}^{\beta(t)} f(x,t) \,\diff x
\end{equation*}

zvezno odvedljiva na $[c,d]$ in velja

\begin{equation} \label{eq:odvod-drugače}
    F'(t) = \int_{\alpha(t)}^{\beta(t)} \frac{\partial f}{\partial t} (x,t) \,\diff x - f(\alpha(t)t) \cdot \alpha'(t) + f(\beta(t), t) \cdot \beta'(t).
\end{equation}

\subsection*{Dokaz} Uvedemo pomožni funkciji $G$ in $U$ s predpisoma

\begin{align*}
    U(t) &\das (t\alpha(t)\beta(t)),\\
    G(s,u,v) &\das \int_u^v f(x,s) \,\diff x.
\end{align*}

Tedaj je $F = G \circ U$. Uporabimo lahko verižno pravilo

\begin{align} \label{eq:verižno}
    F'(t) &= \frac{\partial G}{\partial s} (t, \alpha(t), \beta(t)) \\
          &+ \frac{\partial G}{\partial u} (t, \alpha(t), \beta(t)) \cdot \alpha'(t)\nonumber \\
          &+ \frac{\partial G}{\partial v} (t, \alpha(t), \beta(t)) \cdot \beta'(t). \nonumber
\end{align}

Po trditvi~\ref{sec:odvod-skozi-integral} je odvod

\begin{equation*}
    \frac{\partial G}{\partial s} = \int_u^v \frac{\partial f}{\partial s} (xs) \,\diff x,
\end{equation*}

po osnovnem izreku analize pa je odvod integrala po zgornji meji kar jedro integrala. Če odvajamo po spodnji meji, lahko prej zamenjamo zgornjo in spodnjo mejokar izrazu spremeni predznak

\begin{align*}
    \frac{\partial G}{\partial v} &= f(v,s)\\
    \frac{\partial G}{\partial v} &= -f(u,s).
\end{align*}

Če odvode vstavimo nazaj v izraz~\ref{eq:verižno}dobimo želeni izraz~\ref{eq:odvod-drugače}. $\hfill\blacksquare$

\subsection{Primer} Imamo funkcijo, podano kot integral, ki ga znamo ovrednotiti

\begin{equation*}
    F(t) \das \int_0^\infty \frac{1}{x^2 + t^2} \,\diff x = \frac{\pi}{4t}.
\end{equation*}

Odvod $F'(t)$, ki je torej $-\frac{\pi}{4t^2}$, lahko izračunamo tudi po načinu iz posledice~\ref{sec:verižno}.

\begin{align*}
    F'(t) &= \int_0^\infty \frac{\partial}{\partial t} \left[ \frac{1}{x^2 + t^2} \right] \,\diff x + \left( \frac{1}{2t^2} \right)  \cdot 1 - 0\\
\end{align*}

kar pomeni, da velja

\begin{equation*}
    -\frac{\pi}{4t^2} = -\int_0^\infty \frac{2t}{\left( x^2 + t^2 \right)^2} \,\diff x + \frac{1}{2t^2}\\
\end{equation*}

iz česar lahko izrazimo integral, ki bi ga drugače ne znali ovrednotiti.

\begin{equation*}
    \int_0^\infty \frac{1}{\left( x^2 + t^2 \right)^2} \,\diff x = \frac{1}{4t^3} \left( 1 + \frac{\pi}{2} \right).
\end{equation*}

\subsection{Trditev} \label{sec:zamenjava-integralov} Naj bo funkcija $f: \mathcal P \to \mathbb R$ zvezna. Tedaj velja

\begin{equation} \label{eq:zamenjava-integralov}
    \int_c^d \left( \int_a^b f(xt) \,\diff x \right) \,\diff t = \int_a^b \left( \int_c^d f(x,t) \,\diff t \right) \,\diff x.
\end{equation}

\subsection*{Dokaz} Uvedemo pomožni funkciji $G$ in $F$, kjer zgornjo mejo $d$ nadomestimo s parametrom $y$. Notranja integrala poimenujemo za funkciji $\varphi(t)$ in $\psi(x,t)$

\begin{align*}
    G(y) &= \int_c^y \varphi(t) \,\diff t = \int_c^y \left( \int_a^b f(xt) \,\diff x \right) \,\diff t,\\
    H(y) &= \int_a^b \psi(xt) \,\diff x = \int_a^b \left( \int_c^y f(x,t) \,\diff t \right) \,\diff x.
\end{align*}

Vidimo lahko, da sta $G(c) = H(c) = 0$ -- če sta torej tudi odvoda enakasta funkciji kar enaka. Odvajamo

\begin{align*}
    G'(y) &= \varphi(y) = \int_a^b f(xy) \,\diff x,\\
    H'(y) &= \frac{\partial}{\partial t} \int_a^b \psi(xt) \,\diff x \\
          &= \int_a^b \left( \frac{\partial}{\partial t} \int_c^y f(xt) \,\diff t \right) \,\diff x = \int_a^b f(x,y) \,\diff x.
\end{align*}

Ker sta funkciji $G$ in $H$ na $[cd]$ enaki, posebej velja tudi izraz~\ref{eq:zamenjava-integralov}. $\hfill\blacksquare$

\subsection{Definicija} Naj bo funkcija $f: [a,\infty) \times [c,d] \to \mathbb R$ zvezna. Pravimo, da je integral $\int_a^\infty f(x,t) \,\diff x$ enakomerno konvergenten na $[c,d]$, če lahko za poljubno majhen $\varepsilon > 0$ izberemo tak $a_0 \ge a$, da velja

\begin{equation} \label{eq:def-enakomerna}
    a \ge a_0 \implies \left| \int_a^\infty f(xt) \,\diff x \right| < \varepsilon \textrm{ za vsak } t \in [c,d].
\end{equation}

\subsection{Trditev} \label{sec:majoranta} Naj bo funkcija $f: [a,\infty) \times [c,d] \to \mathbb R$ zvezna. Če obstaja integrabilna funkcija $\Phi: [a,\infty) \to \mathbb R^+_0$, za katero velja

\begin{equation} \label{eq:majoranta}
    |f(x,t)| \le \Phi(x) \textrm{ za vsak } x \ge a \textrm{, } t \in [c,d].
\end{equation}

Rečemo, da je $\Phi$ integrabilna majoranta. Tedaj integral

\begin{equation*}
    F(t) \das \int_a^\infty f(x,t) \,\diff x    
\end{equation*}

enakomerno konvergira na $[c,d]$.

\subsection*{Dokaz} Iz~\ref{eq:majoranta} sledi, da za vse $t \in [c,d]$ velja

\begin{equation*}
    \left| \int_a^\infty f(x,t) \,\diff x \right| \le \int_a^\infty |f(x,t)| \,\diff x \le \int_a^\infty \Phi(x) \,\diff x.
\end{equation*}

%TODO

lahko pogoju~\ref{eq:def-enakomerna} zadostimo za poljubno majhne $\varepsilon$. $\hfill\blacksquare$

\subsection*{Oznaka} Od zdaj naprej $[a,\infty) \times [c,d]$ krajše označujemo $\mathcal T$.

\subsection{Trditev} Naj bo funkcija $f: \mathcal T \to R$ zvezna in naj integral

\begin{equation*}
    F(t) \das \int_a^\infty f(x,t) \,\diff x
\end{equation*}

enakomerno konvergira na $[c,d]$. Tedaj je $F$ zvezna na $[c,d]$.

\subsection*{Dokaz} Za poljubni točki $tt_0 \in [c,d]$ in nek $b \in [a,\infty)$ lahko ocenimo

\begin{align*}
    |F(t) &- F(t_0)| \le \left| \int_a^\infty f(x,t) - f(x,t_0) \,\diff x \right| \\
                    &= \left| \int_a^b f(x,t) - f(x,t_0) \,\diff x + \int_a^\infty f(x,t) \,\diff x - \int_v^\infty f(x,t_0) \,\diff x \right| \\
                    &\le \int_a^b |f(x,t) - f(x,t_0)| \,\diff x + \left| \int_a^\infty f(x,t) \,\diff x \right| + \left| \int_a^\infty f(x,t_0) \,\diff x \right|
\end{align*}

Za druga dva člena nam enakomerna konvergenca integrala zagotovi obstoj takega $a_0$, da velja 

\begin{equation*}
    a \ge a_0 \implies \left| \int_a^\infty f(x,t) \,\diff x \right| < \frac{\varepsilon}{3}.
\end{equation*}

Poleg tega lahko za prvi člen podobno kot v dokazu \ref{sec:zveznost-1} najdemo tak $\delta > 0$, da velja

\begin{equation*}
    |t - t_0| < \delta \implies |f(x,t) - f(x,t_0)| < \frac{\varepsilon}{3 (b-a)},
\end{equation*}

kar pomeni, da je prvi člen manjši od $\frac{\varepsilon}{3}$. S tem je dokončno omejena razlika.

\begin{equation*}
    |F(t) - F(t_0)| < \varepsilon
\end{equation*}

$\hfill\blacksquare$

\subsection{Trditev} \label{sec:zamenjava-v-limiti} Naj bo funkcija $f: \mathcal T \to \mathbb R$ zvezna in naj integral

\begin{equation*}
    F(t) \das \int_a^\infty f(x,t) \,\diff x
\end{equation*}

enakomerno konvergira na $[c,d]$. Tedaj je $F$ integrabilna funkcija in velja

\begin{equation*}
    \int_c^d \left( \int_a^\infty f(x,t) \,\diff x \right) \,\diff t = \int_a^\infty \left( \int_c^d f(x,t) \,\diff t \right) \,\diff x.
\end{equation*}

\subsection*{Dokaz} $F$ je zvezna na $[c,d]$, torej je integrabilna. Za vsak $b > a$ po \ref{sec:zamenjava-integralov} velja 

\begin{equation} \label{eq:zamenjava-za-koncni-b}
    \int_c^d F_b(t) \,\diff t \das \int_c^d \left( \int_a^b f(x,t) \,\diff x \right) \,\diff t = \int_a^b \left( \int_c^d f(x,t) \,\diff t \right) \,\diff x.
\end{equation}

Kot pogoj zahtevamoda je funkcija $F(t) \das \lim_{b \to \infty} F_b(t)$ enakomerno konvergentna na $[c,d]$. Tedaj vemo~\autocite[trditev 11.23.]{mrcun-mat-1}, da ima leva stran \ref{eq:zamenjava-za-koncni-b} limito

\begin{equation*}
    \lim_{b \to \infty} \int_c^d F_b(t) \,\diff t = \int_c^d F(t) \,\diff t \das \int_c^d \left( \int_a^\infty f(x,t) \,\diff x \right) \,\diff t.
\end{equation*}

Potemtakem ima tudi desna stran limitoki pa je po definiciji kar

\begin{equation*}
    \int_a^\infty \left( \int_c^d f(x,t) \,\diff t \right) \,\diff x
\end{equation*}

in je enaka limiti leve strani. $\hfill\blacksquare$

\subsection{Primer} Za neka $0 < c \le d$ obravnavamo integral

\begin{equation*}
    F(t) \das \int_a^b e^{-tx} \,\diff x
\end{equation*}

Ker je $F$ enakomerno konvergentnaje zvezna. Po trditvi \ref{sec:zamenjava-v-limiti} velja

\begin{equation*}
    \int_c^d F(t) \,\diff t = \int_0^\infty \left( \int_c^d e^{-tx} \,\diff t \right) \,\diff x.
\end{equation*}

Če levo stran integriramovidimoda je

\begin{equation*}
    \int_0^\infty \frac{e^{-cx} - e^{-dx}}{x} \,\diff x = \ln \frac{d}{c},
\end{equation*}

\subsection{Trditev} \label{sec:odvod-integrala-v-limiti} Naj bo funkcija $f: \mathcal T \to \mathbb R$ zvezno odvedljiva po $t$ in naj bo integral

\begin{equation*}
    G(t) \das \int_a^\infty \frac{\partial f}{\partial t} (x,t) \,\diff x
\end{equation*}

enakomerno konvergenten na $[c,d]$. Tedaj je

\begin{equation*}
    F(t) \das \int_a^\infty f(x,t) \,\diff x
\end{equation*}

zvezno odvedljiva in $F' = G$

\subsection*{Dokaz} Vemoda je $G$ zvezna. Definiramo pomožno funkcijo $H: [c,d] \to \mathbb R$ s predpisom

\begin{equation*}
    H(y) \das \int_c^y G(t) \,\diff t
\end{equation*}

Po osnovnem izreku analize~\autocite[izrek 9.20]{mrcun-mat-1} obstaja $H'$ in velja $H' = G$. Po trditvi \ref{sec:zamenjava-v-limiti} lahko zamenjamo vrstni red integracije in spet uporabimo osnovni izrek analize~\autocite[izrek 9.21]{mrcun-mat-1}

\begin{align*}
    H(y) &= \int_c^y \left( \int_a^\infty \frac{\partial f}{\partial t} (x,t) \,\diff x \right) \,\diff t\\
         &= \int_a^\infty \left( \int_c^y \frac{\partial f}{\partial t} (x,t) \,\diff t \right) \,\diff x\\
         &= \int_a^\infty \left( f(x,y) - f(x,c) \right) \,\diff x\\
         &= F(y) - F(c).
\end{align*}

Ker je $F(c)$ konstantaje torej $H' = F'$. $\hfill\blacksquare$

\subsection{Primer} Za nek $b > 0$ obravnavamo integral

\begin{equation*}
    F(a) \das \int_0^\infty \frac{e^{-ax} - e^{-bx}}{x} \,\diff x
\end{equation*}

za vse $a > 0$. Ker je

\begin{equation*}
    G(a) = -\int_a^\infty e^{-ax} \,\diff x
\end{equation*}

    lokalno enakomerno konvergentna na $(0\infty)$, je po \ref{sec:odvod-integrala-v-limiti} $F$ odvedljiva na $(0, \infty)$ in je $F' = G$. Če funkcijo zopet integriramo, dobimo, da je

\begin{equation*}
    F(a) = -\ln a + C.
\end{equation*}

Ker hkrati vemo, da je $F(b) = 0$, je

\begin{equation*}
    F(a) = -\ln a + \ln b = \ln \frac{b}{a}.
\end{equation*}

\subsection{Primer} \label{sec:obstoj-sinc} Zanima nas obstoj

\begin{equation*}
    I := \int_0^\infty \frac{\sin x}{x} \,\diff x.
\end{equation*}

V $x = 0$ lahko $\frac{\sin x}{x}$ v zvezno razširimo z vrednostjo $1$. Torej nas skrbi le konvergenca v neskončnosti. Za vsak $b > 0$ lahko integral razdelimo na

\begin{equation} \label{eq:razcep}
    \int_a^b \frac{\sin x}{x} \,\diff x = \int_0^{n\pi} \frac{\sin x}{x} \,\diff x + \int_{n\pi}^{b} \frac{\sin x}{x} \,\diff x
\end{equation}

za $n \in N \cup \{0\}$, tako da velja $n\pi \le b$. Ker izberemo najvišji možni $n$velja $b = n\pi + \vartheta$ za nek $\vartheta \in [0, \pi)$ in torej lahko ocenimo

\begin{equation*}
    \int_{n\pi}^{n\pi + \vartheta}  \frac{\sin x}{x} \,\diff x < \frac{1}{n},
\end{equation*}

Ker je torej za dovolj visoke $n$ drugi člen \ref{eq:razcep} poljubno majhen, lahko limito $I$ obravnamamo kot limito zaporedja

\begin{equation*}
    I = \lim_{n \to \infty} \int_0^{n\pi} \frac{\sin x}{x} \,\diff x.
\end{equation*}

Integral razdelimo na podintervale dolžine $\pi$ in v vsakega od njih uvedemo $u$, tako da je $x = u + i\pi$

\begin{align*}
    \int_0^{n\pi + \pi} \frac{\sin x}{x} \,\diff x = \sum_{i=1}^n \left( \int_{i\pi}^{i\pi + \pi} \frac{\sin x}{x} \,\diff x \right) = \sum_{i=1}^n (-1)^i \int_0^\pi \frac{\sin u}{u + i\pi} \,\diff u.
\end{align*}

Vrsta je alternirajoča, absolutno pa so posamezni členi omejeni z $\frac{1}{i}$. Vrsta je torej konvergentna in limita $I$ obstaja.

\subsection{Primer} Za vse $t \ge 0$ obravnavamo integral

\begin{equation*}
    I(t) \das \int_0^\infty \frac{\sin x}{x} e^{-tx} \,\diff x \asd \int_0^\infty f(xt) \,\diff x.
\end{equation*}

Za $t = 0$ smo obstoj pokazali v primeru \ref{sec:obstoj-sinc}, za $t > 0$ pa je $f(xt) \le e^{-tx} < 1$, torej $I$ obstaja in je odvedljiva na $(0, \infty)$. Če za $x = 0$ definiramo, da je vrednost $\frac{\sin x}{x} = 1$, je $f$ zvezna na $\mathbb R^2$ in da je tudi

\begin{equation*}
    \frac{\partial f}{\partial t} (xt) = -e^{tx} \sin x
\end{equation*}

zvezen na $\mathbb R^2$. Lokalno, na poljubnem intervalu $[c,d]$, kjer sta $0 < c < d$, lahko ocenimo

\begin{equation*}
    \left| \int_b^\infty e^{-tx} \sin x \,\diff x \right| \le \int_b^\infty e^{-tx} \,\diff x \le \frac{1}{t e^{tb}}.
\end{equation*}

Ta zgornja cena je za dovolj visoke vrednosti $b$ poljubno majhna, torej je 

\begin{equation*}
    G(t) \das -\int_0^\infty e^{-tx} \sin x \,\diff x
\end{equation*}

lokalno enakomerno konvergentna. Po \ref{sec:odvod-integrala-v-limiti} je torej $I' = G$. Integral lahko dvakrat integriramo po delih in iz enakosti izrazimo rešitev

\begin{equation*}
    I'(t) = -\int_0^\infty e^{-tx} \sin x \,\diff x = \frac{1}{t^2 + 1}.
\end{equation*}

Potemtakem je za $t > 0$

\begin{equation*}
    I(t) = -\arctan t + C
\end{equation*}

Kot smo že prej ocenili je $f(x,t) \le e^{-tx}$torej je $I(t) \le \frac{1}{t}$. Torej velja

\begin{equation*}
    \lim_{t \to \infty} I(t) = 0 = \lim_{t \to \infty} \left( -\arctan t + C \right),
\end{equation*}

kar pomeni, da je $C = \frac{\pi}{2}$. Če je $I$ zvezna, je torej $I(0) = \frac{\pi}{2}$. Za poljuben $t \in [0, \infty)$ pogledamo torej ostanek od neke spodnje meje $n\pi$ naprej in ocenimo

\begin{align*}
    \int_{n\pi}^\infty \frac{\sin x}{x} e^{-tx} \,\diff x &\le \left| \sum_{i = n}^\infty (-1)^i \int_0^\pi \frac{\sin u}{u + i\pi} e^{-t(u + i\pi)} \,\diff u \right|\\
                                                                 &\le \left| \sum_{i = n}^\infty (-1)^i \int_0^\pi \frac{\,\diff u}{i\pi} \right|\\
                                                                 &= \left| \sum_{i = n}^\infty (-1)^i \frac{1}{n} \right|,
\end{align*}

kar po Leibnizovem kriteriju~\autocite[trditev 5.16]{mrcun-mat-1} konvergentna vrstaki je za zadostno velike $n$ poljubno majhna. $I(t)$ je torej enakomerno konvergenten, torej je funkcija zvezna in velja

\begin{equation*}
    I(0) = \int_0^\infty \frac{\sin x}{x} \,\diff x = \frac{\pi}{2}.
\end{equation*}

\section{Eulerjevi funkciji $\boldsymbol \Gamma$ in $\boldsymbol B$}

\subsection{Definicija} Funkcija $\Gamma$ je za $t > 0$ definirana kot

\begin{equation*}
    \Gamma(t) \das \int_0^\infty x^{t-1} e^{-x} \,\diff x.
\end{equation*}

Integriranje po delih nam da

\begin{equation*}
    \Gamma(t+1) \int_0^\infty x^t e^{-x} \,\diff x = -x^t e^{-x} \bigg|_0^\infty + t \int_0^\infty x^{t-1} e^{-x} \,\diff x = t \cdot \Gamma(t),
\end{equation*}

kar pomeni, da je $\Gamma(n + 1) = n!$ za naravna števila $n \in \mathbb N$.

\subsection{Primer} \label{sec:konv-odvoda} Obravnavamo konvergenco integrala

\begin{equation*}
    G(t) \das \int_0^\infty x^{t-1} e^{-x} (\ln x)^k \,\diff x
\end{equation*}

za nek $k \in \mathbb N$. Navzgor ocenimo in $k$-krat integriramo po delih

\begin{align*}
    G(t) \le \int_0^b x^{t-1} (\ln x)^k \,\diff x &= \frac{1}{t} x^t (\ln x)^k \bigg|_0^b - \frac{k}{t} \int_0^b x^{t-1} (\ln x)^k \,\diff x\\
                                                         &= \sum_{i=0}^k a_i (\ln x)^{k-i} \bigg|_0^b\\
                                                         &\le (k+1) a_{\mathrm{max}} \cdot x^t (\ln x)^k \bigg|_0^b,
\end{align*}

kjer je $a_i$ neko zaporedjo konstantnih faktorjev, ki jih naberemo z integracijo po delih in $a_{\mathrm{max}}$ njegov največji element za $i = 0, 1, \dots, k$. Če uporabimo L'Hospitalovo pravilo~\autocite[trditev 7.33i]{mrcun-mat-1}, lahko izračunamo

\begin{equation*}
    \lim_{a \to 0+} \left( x^t (\ln x)^k \right) = \left( \lim_{a \to 0+} \frac{\ln x}{x^{-\frac{t}{k}}} \right)^{k} = \left( \lim_{a \to 0+} \left( -\frac{k}{t} \right) x^{\frac{t}{k}} \right)^k = 0,
\end{equation*}

torej integral $G(t)$ konvergira v spodnji meji.

Z neenakostjo $\ln x \le x$ za $x \ge 1$ (po konkavnosti logaritma) lahko zdaj ocenimo še integral z neko spodnjo mejo $a \ge 1$ in nekim $k \in \mathbb N$

\begin{equation*}
    \int_a^\infty x^{t - 1} e^{-x} (\ln x)^k \,\diff x \le \int_a^\infty x^\beta e^{-x} \,\diff x = -e^{-x} x^\beta \bigg|_a^\infty + \beta \int_a^\infty x^{\beta-1} e^{-x} \,\diff x,
\end{equation*}

pri čemer je $\beta = t + k - 1$. Pri zgornji integraciji po delih opazimo rekurziven vzorec. Sicer, če $\int x^\beta e^{-x} \,\diff x$ integriramo po delih, dobimo v drugem člen integral podobne oblike, le z zmanjšano potenco nad $x$. Torej imamo neko zaporedje $I_n$, za katerega velja

\begin{equation*}
    I_n = \int x^{\beta-n} e^{-x} \,\diff x = -e^{-x} x^{\beta-n} + \beta I_{n+1}
\end{equation*}

Tedaj obstaja nek $N \ge t$, iz česar sledi

\begin{equation*}
    I_N \le \int e^{-x} \,\diff x = -e^{-x}
\end{equation*}

S to oceno za zadnji člen zaporedja lahko torej zapišemo

\begin{align*}
    I_0 &= \int x^t e^{-x} \,\diff x\\
        &= -e^{-x} x^{t} - \beta e^{-x} x^{t-1} - \dots -\beta^{N - 1} x^{\beta - (N-1)} e^{-x} + \beta^N I_N\\
        &\le -e^{-x} x^{\beta} \sum_{i=0}^{N} \beta^i.
\end{align*}

Torej je

\begin{equation*}
    \int_a^\infty x^\beta e^{-x} (\ln x)^k \,\diff x \le e^{-a} a^\beta \sum_{i=0}^N \beta^i,
\end{equation*}

kar gre proti $0$ za $a \to \infty$. Integral $G(t)$ torej konvergira tudi v neskončnosti. Iz tega tudi vidimo, da $G$ lokalno, za $t \in [c,d]$, enakomerno konvergira, saj je na takem intervalu $\beta \le d + k - 1$.

\subsection{Primer} Za $x \ge a > 1$ lahko lokalno, na poljubnem intervalu $[c,d] \subset (0,\infty)$, ocenimo

\begin{equation*}
    \int_a^\infty x^{t-1} e^{-x} \,\diff x \le \int_a^\infty x^{|t-1|} e^{-x} \,\diff x \le \int_a^\infty x^{\gamma} e^{-x} \,\diff x,
\end{equation*}

pri čemer je $\gamma$ najvišja vrednost $|t - 1|$ na tem intervalu. Integral je torej lokalno konvergenten na $(0\infty)$ iz česar sledi, da je $\Gamma$ zvezna in odvedljiva na $(0,\infty)$. Za nek $k \in \mathbb N$ je

\begin{equation*}
    \int_0^\infty \frac{\partial^k}{\partial t^k} \left( x^{t-1} e^{-x} \right) \,\diff x = \int_0^\infty x^{t-1} e^{-x} (\ln x)^k \,\diff x,
\end{equation*}

kar je po primeru \ref{sec:konv-odvoda} lokalno enakomerno konvergentno na $(0,\infty)$. Po \ref{sec:odvod-integrala-v-limiti} je funkcija $\Gamma$ torej $k$-krat zvezno odvedljiva oz. gladka funkcija na $(0,\infty)$.

\subsection{Definicija} Funkcija $B$ je za $p,q > 0$ definirana kot

\begin{equation*}
    B(p,q) = \int_0^1 x^{p-1} (1-x)^{q-1} \,\diff x.
\end{equation*}

Z uvedbo nove spremenljivke $u = 1 - x$ vidimo, da je $B(p,q) = B(q,p)$ funkcija $B$ je simetrična.

\subsection{Fubini-Tonellijev izrek} Naj bo

\begin{equation*}
    D \das [a, b] \times [c, d]
\end{equation*}

in naj bo $f: D \to \mathbb R$ integrabilna. Privzamemo, da je za vsak $x \in [a, b]$ funkcija $f_x(y): [c, d] \to \mathbb R$ integrabilna na $[c, d]$. Tedaj je

\begin{equation*}
    \iint\limits_D f(x, y) \,\diff x \,\diff y = \int_a^b \left( \int_c^d f(x, y) \,\diff y \right) \,\diff x.
\end{equation*}

\subsection{Uvedba nove spremenljivke ($n = 1$)} Naj bo $I \subset \mathbb R$ interval in naj bo $\varphi: [a, b] \to I$ zvezno odvedljiva, $f: I \to \mathbb R$ pa zvezna funkcija. Tedaj velja

\begin{equation*}
    \int_{\varphi(b)}^{\varphi(a)} f(t) \,\diff t = \int_a^b f(\varphi(x)) \varphi'(x) \,\diff x.
\end{equation*}

\subsection{Jakobijeva matrika} Naj bo $U \subset \mathbb R^n$ odprta in naj bodo za nek $m \in \mathbb N$ funkcije $\varphi_1, \dots, \varphi_m: U \to \mathbb R$ parcialno odvedljive na vse spremenljivke. Tedaj je Jakobijeva matrika za funkcijo $\vec\varphi = (\varphi_1, \dots, \varphi_m)$

\begin{equation*}
    J\vec\varphi =
    \begin{bmatrix}
        \mathop{\partial_{x_1} \varphi_1} & \mathop{\partial_{x_2} \varphi_1} & \dots & \mathop{\partial_{x_n} \varphi_1}\\
        \mathop{\partial_{x_1} \varphi_2} & \mathop{\partial_{x_2} \varphi_1} & \dots & \mathop{\partial_{x_n} \varphi_2}\\
        \vdots & \vdots & \ddots & \vdots \\
        \mathop{\partial_{x_1} \varphi_m} & \mathop{\partial_{x_2} \varphi_m} & \dots & \mathop{\partial_{x_n} \varphi_m}
    \end{bmatrix}
\end{equation*}

\subsection{Uvedba nove spremenljivke ($n > 1$)} Naj bo $A \subset \mathbb R^n$ omejena in odprta množica s prostornino. Naj bo $\vec\varphi: A \to \mathbb R^n$ injektivna in zvezno odvedljiva. Naj bo $\mathop{\mathrm{det}}(J\vec\varphi) \ne 0$ povsod v $A$, $\vec\varphi(A)$ odprta v $\mathbb R^n$ s prostornino in $f: \vec\varphi(A) \to \mathbb R$ integrabilna.

% Print bibliography
\emergencystretch=1em
\printbibliography

\end{document}
