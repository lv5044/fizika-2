% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{subsection}
\setlength{\skip\footins}{1.5cm}

\title{Transmisijska linija}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Za prenos visokofrekvenčnih signalov se pogosto uporablja t. i. koaksialne kable. Njihova sestava zmanjša vpliv zunanjih polj in šibek signal (televizija, internet, meritve podatkov, ipd.) zaščitijo pred motnjami (tranformatorji, telekomunikacijski signali, ipd.).Količina, ki označuje koaksialen kabel, je njegova karakteristična impedanca. Ker mora biti robnim pogojem za valovno enačbo zadoščeno povsod, moramo kabel zaključiti s primernim uporom, enakim karakteristični impedanci kabla. V nasprotnem primeru opazimo ob zaključku kabla znaten odboj.

Za enačbi, ki opisujeta idealizacijo koaksialnega kabla

\begin{align*}
    \frac{\partial^2 I}{\partial x^2} &= L' C' \frac{\partial^2 I}{\partial t^2}, \\
    \frac{\partial^2 U}{\partial x^2} &= L' C' \frac{\partial^2 U}{\partial t^2},
\end{align*}

je splošna rešitev oblike

\begin{align*}
    I(x, t) = \left( Ae^{ikx} + Be^{ikx} \right) e^{i\omega t}, \\
    U(x, t) = \left( Ce^{ikx} + De^{ikx} \right) e^{i\omega t},
\end{align*}

pri čemer koeficienti $A, B, C, D$ niso neodvisni temveč povezani preko

\begin{equation*}
    \frac{\partial U}{\partial x} = -L' \frac{\partial I}{\partial t},
\end{equation*}

kar nam da

\begin{equation*}
    \left( Ce^{ikx} - De^{-ikx} \right) e^{i\omega t} = Z_0 \left( A e^{ikx} + B e^{-ikx} \right) e^{i\omega t},
\end{equation*}

pri čemer je $Z_0 = \frac{\omega L'}{k} = \sqrt{\frac{L'}{C'}}$ karakteristična impedanca kabla. V naši eksperimentalni postavitvi kabel napajamo z izmeničnim tokom z amplitudo $I_0$. Na koncu kabla ($x = d$) priključimo znano Ohmnsko upornost $R_z$, ki nam določi razmerje med tokom in napetostjo na tem mestu. Pri času 0 torej dobimo robne pogoje za $x = 0$ in $x = d$ -- predstavlja jih sistem enačb

\begin{align*}
    A + B &= I_0, \\
    Ae^{ikd} + Be^{ikd} &= \frac{Cee^{ikd} - De^{ikd}}{R_z}.
\end{align*}

Če kabel zaključimo z $R_z = Z_0$, lahko sistem rešimo in dobimo $B = 0$, kar pomeni, da nimamo odboja.

\begin{figure}
    \centering
    \import{}{setup.pdf_tex}
    \caption{Eksperimentalna postavitev, ki jo uporabljamo.}
    \label{fig:setup}
\end{figure}

\section{Naloga}

\begin{enumerate}
    \item Izmeri hitrost elektromagnetnega valovanja v kablu in določi dielektrično konstanto izolacije $\varepsilon$.
    \item Izmeri in nariši diagram (vse meritve na en graf) amplitude napetosti na začetku kabla $U_0$ kot funkcijo frekvence $\omega$ za vse zaključne impedance kabla, ki jih imaš na voljo. Določi tudi ekstreme -- njihove frekvence in amplitude.
    \item Določi karakteristični upor kabla $Z_0$ iz meritve frekvenčne odvisnosti in izračunaj $L'$ in $C'$ iz znanih dimenzij kabla. Primerjaj jih z znanimi podatki za uporabljeni kabel.
\end{enumerate}

\section{Meritve in račun}

Vključimo osciloskop in generator signala in prvo izmerimo napetost v točki A.

\begin{figure}
    \centering
    \includegraphics{RMS.pdf}
    \caption{Signal kot izmerjen v točki A (skica \ref{fig:RMS}).}
    \label{fig:RMS}
\end{figure}

Nato sondo priključimo na točko B in prečešemo območje frekvenc, ki vključuje dve ojačitvi brez zaključnega upora in eno ojačitev pri večjih zaključnih uporih. Za izračun relativne permitivnost $\varepsilon$ uporabimo dejstvo, da je pri prvi ojačitvi valovna dolžina signala ravno enaka $\ell / 4$, pri čemer je $\ell$ dolžina kabla

\begin{equation*}
    \ell = (704 \pm 1)\,\si{cm}.
\end{equation*}

Valovno dolžino $\lambda_1 = c / \nu_1$ pri ojačitvi najdemo kot maksimumum vsote resonančnih krivulj za zaključne upore $0, \SI{5}{\ohm}, \SI{10}{\ohm}, \SI{15}{\ohm}, \SI{22}{\ohm}$ (slika \ref{fig:max}).

\begin{figure}
    \centering
    \includegraphics{max.pdf}
    \caption{Prva ojačitev signala. Gledamo maksimum vsote resonančnih krivulj za zaključne upore $0, \SI{5}{\ohm}, \SI{10}{\ohm}, \SI{15}{\ohm}, \SI{22}{\ohm}$.}
    \label{fig:max}
\end{figure}

Iz tega lahko izračunamo hitrost signala v kablu

\begin{equation*}
    c = 4\nu_1 \ell
\end{equation*}

in relativno permitivnost

\begin{equation*}
    \varepsilon = \left( \frac{c_0}{c} \right)^2,
\end{equation*}

pri čemer je $c_0$ hitrost svetlobe v vakumu. Izračunamo relativni $\varepsilon$

\begin{equation*}
    \varepsilon = 2.9 \pm 1.0.
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{resonance.pdf}
    \caption{Resonančne krivulje pri različnih zaključkih kabla. Prikazana je RMS napetost med ozemljitvijo in točko $B$ (skica \ref{fig:setup})}
    \label{fig:p-fit}
\end{figure}

Na sliki \ref{fig:setup} lahko vidimo, da je krivulja pri zaključnem uporu $\SI{51}{\ohm}$ najbolj položna, torej se signal na koncu kabla najmanj odbije. To lahko nekoliko podkrepimo tudi s tabelo \ref{tab:std}, v kateri so izračunane standardne deviacije resonančne krivulje pri različnih zaključnih uporih. Ker iz teorije pričakujemo, da odboja pri $R_z = Z_0$ ni, ocenimo, da je impedanca kabla

\begin{equation*}
    Z_0 = (51 \pm 5)\,\si{\ohm}.
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{c|r}
        $R_z\,[\si{\ohm}]$ & $\sigma_U\,[\si{V}]$ \\
        \hline
        0 & 0.41 \\
        5 & 0.24 \\
        10 & 0.14 \\
        15 & 0.13 \\
        22 & 0.09 \\
        33 & 0.05 \\
        51 & 0.03 \\
        100 & 0.07 \\
        215 & 0.14 \\
        560 & 0.23 \\
        $\infty$ & 0.37
    \end{tabular}
    \caption{Standarna deviacija resonančne krivulje -- napetosti -- pri različnih zaključnih uporih.}
    \label{tab:std}
\end{table}

Specifična kapacitivnost kabla je

\begin{equation*}
    C' = \frac{2\pi\varepsilon\varepsilon_0}{\ln\left( \frac{D}{d} \right)},
\end{equation*}

pri čemer je $\varepsilon_0$ permitivnost vakuma, $D = (2.95 \pm 0.02)\,\si{mm}$ znan zunanji premer dielektrika in $d = (0.80 \pm 0.02)\,\si{mm}$ znan premer notranjega vodnika.

Kapacitivnost kabla je

\begin{equation*}
    L' = \frac{\mu_0}{2\pi} \ln\left( \frac{D}{d} \right),
\end{equation*}

pri čemer je $\mu_0$ permiabilnost vakuma. Izračunamo

\begin{align*}
    C' &= (0.13 \pm 0.04)\,\si{nF/m}, \\
    L' &= (0.261 \pm 0.005)\,\si{\mu H/m}.
\end{align*}

Iz tega lahko potem izračunamo impedanco kabla $Z_0 = \sqrt{\frac{L'}{C'}}$

\begin{equation*}
    Z_0 = (45 \pm 7)\,\si{\ohm},
\end{equation*}

kar se znotraj okvira napake ujema z impedanco, ki smo jo ocenili kot zaključni upor pri najbolj položni resonančni krivulji.

\end{document}
