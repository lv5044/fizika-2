% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}

% Differential
\newcommand{\diff}{\mathrm{d}}

\title{Silicijeva dioda}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Fotodioda je visoko elektronski element, v katerem vpadna svetloba preko fotoefekta povzroči elektronov tok v smeri N-dopirane plasti, torej električni tok v nasprotni, zaporni zmeri. Energija fotona $h\nu$ se razdeli po bilanci
\begin{equation*}
    h\nu = W_{\mathrm{v}} + \frac{m_e v^2}{2},
\end{equation*}
kjer je prvi člen vezavna energija, drugi pa kinetična energija elektrona po tem, ko se osvobodi~\footnote{To je hitrost v vakumu pri zunanjem fotoefekt, t. j. ko elektron zapusti snov}. Če fotoefekt uporabljamo za detektiranje fotonov, vezavna energija omejuje frekvenco fotonov, ki jih lahko zaznamo. Na površini so tipične vezavne energije reda $\SI{1}{eV}$, torej lahko s fotoefektom detektiramo valovne dolžine krajše od $\SI{1000}{nm}$.

\begin{figure}
    \centering
    \import{}{diode-diagram.pdf_tex}
    \caption{Skica PID diode.}
    \label{fig:skica}
\end{figure}

Pri poskusu uporabljamo silicijevo PIN fotodiodo (slika~\ref{fig:skica}), v kateri svetloba vstopi skozi tanko P-dopirano plast, absorbira pa se v sredinski I plasti, ki je narejena iz materiala brez nečistoč. Difuzijsko polje v plasti potegne elektron v N-plast, kar povzroči električni tok. Tako lahko s fotocelico iz svetlobnega toka dobimo električno energijo (sončne celice). Če zraven na diodo v zaporni smeri pritisnemo neko napetost, je odvisnost električnega toka v zaporni smeri in jakosti svetlobe skoraj povsem linearna, kar pogosto pomeni lažjo izvedbo detektorjev.

\section{Naloga}

\begin{enumerate}
    \item Izmeri električno karakteristiko $I(U)$ fotodiode v temi in pri različnih osvetlitvah. Meri v obeh načinih, z zunanjim napajanjem, kjer lahko izmeriš celotno karakteristiko, in v fotogalvanskem načinu, kjer je možna meritev le v enem kvadrantu odvisnost $I(U)$.
    \item Nariši en sam graf odvisnosti $I(U)$, kjer je parameter osvetljenost fotodiode, za vse meritve z zunanjim napajanjem in posebej še za meritve v fotogalvanskem načinu. Iz diagrama v fotogalvanskem načinu določi, kolikšne upore bi morali priključiti na fotodiodo ob uporabljenih osvetlitvah, da bi se na uporu trošila kar največja električna moč.
    \item Oceni izkoristek svetleče diode (LED), ki jo uporabljaš kot svetlobni izvor.
\end{enumerate}

\section{Meritve in račun}

Fotodiodo priključimo na zunanje napajanje in prižgemo LED svetilo. Pri različnih oddaljenostih LED (tabela~\ref{tab:d}) pomerimo karakteristiko diode $I(U)$. Meritve so prikazane na sliki~\ref{fig:split}.

\begin{table}[h]
    \centering
    \begin{tabular}{c}
        $d\,[\si{mm}]$ \\
        \hline
        0 \\
        10 \\
        20 \\
        40 \\
    \end{tabular}
    \caption{Oddaljenosti LED, pri katerih pomerimo karakteristiko diode.}
    \label{tab:d}
\end{table}

\begin{figure}
    \centering
    \includegraphics{characteristic-split.pdf}
    \caption{Karakteristika diode pri različnih oddaljenostih LED od diode. Rdeča pika označuje tok $I_0$, ki ga uporabimo pri izračunu učinkovitosti LED diode.}
    \label{fig:split}
\end{figure}

\begin{figure}
    \centering
    \includegraphics{characteristic.pdf}
    \caption{Karakteristika diode pri različnih oddaljenostih LED od diode.}
    \label{fig:char}
\end{figure}

Nato karakteristiko pomerimo še v fotogalvanskem načinu, t. j. brez zunanjega napajanja (slike~\ref{fig:photo}).

\begin{figure}
    \centering
    \includegraphics{photogalvanic.pdf}
    \caption{Karakteristika diode v fotogalvanskem načinu, brez zunanjega napajanja.}
    \label{fig:photo}
\end{figure}

Izmerimo tudi napetost in tok skozi LED.

\begin{equation*}
    U_\mathrm{LED} = (1.87 \pm 0.01)\,\si{V}.
\end{equation*}

\begin{equation*}
    I_\mathrm{LED} = (23.30 \pm 0.05)\,\si{mA}.
\end{equation*}

Iz grafa v navodilih lahko razberemo, da je spektralna občutljivost $\chi$ pri svetlobi valovne dolžine $\lambda = \SI{650}{nm}$, kakrško oddaja rdeča LED, ki jo uporabljamo

\begin{equation*}
    \chi = (0.45 \pm 0.01)\,\si{A/W}
\end{equation*}

Če učinkovitost LED označimo kot $\eta$ in rečemo, da skozi odprtino v ohiš fotodiode pride $\nu = 0.75 \pm 0.1$ celotnega svetlobnega toka, je tok $I_0$, ki teče skozi fotodiodo brez pritisnjene napetosti (razberemo iz grafa slike~\ref{fig:split} pri $d = 0$ in $U = 0$)

\begin{equation*}
    |I_0| = \chi \nu \eta I_\mathrm{LED} U_\mathrm{LED}.
\end{equation*}

Iz znanega

\begin{equation*}
    I_0 = (-0.7708 \pm 0.0005)\si{mA},
\end{equation*}

izračunamo učinkovitost

\begin{equation*}
    \eta = (5.2 \pm 0.7)\%.
\end{equation*}

\noindent\textbf{Komentar.} Ker sem meritve delal precej na gosto, sem pri zaporni napetosti, pri kateri začne tok strmo naraščati, meritve zaključil. Zato so na sliki~\ref{fig:char} strmo naraščajoči deli slabo vidni. Če bi vajo ponavljal, bi meritveno območje za $U$ razširil za dobre $\SI{0.5}{V}$.


\end{document}
