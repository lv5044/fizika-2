% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{subsection}
\setlength{\skip\footins}{1.5cm}

\title{Feromagnetizem}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Feromagnetni materiali, predvsem kovine kot sta npr. železo in nikelj, imajo relativno permiabilnost $\mu$ mnogo večjo od $1$. Gostota magnetnega polja $B$ je torej znotraj feromagnetnega materiala mnogo večja kot v praznem prostoru okoli materiala. To je posledica spontanega urejanja magnetnih dipolov znotraj makroskopskih domen v snovi. Če snov damo v zunanje magnetno polje, se magnetizira. Domene, usmerjene v smer zunanjega polja, se razširijo na račun ostalih domen, s tem pa se število domen tudi zmanjša.

Ko zunanje magnetno polje ugasnemo, te domene ne izginejo v celoti -- material si \blockquote{zapomne} zunanje magnetno polje. Feromagnetne snovi so tako lahko trajni magneti, ki so magnetizirani tudi brez zunanjega magnetnega polja. Kot to vidimo na sliki \ref{fig:zanka}, odvisnost $B(H)$ ni preprosta funkcija -- polje v materialu je poleg zunanjega polja odvisno tudi od zgodovine materiala.


\begin{figure}[h]
    \centering
    \import{}{hysteresis-loop.pdf_tex}
    \caption{Začetna magnetilna krivulja in nasičena histerezna zanka. Prej nenamagneten feromagnet (v izhodišču 0) se v prisotnosti zunanjega polja namagneti po poti (1-2-3). Ko zunanje polje postopoma umaknemo, v materialu ostane remanentna gostota polja $B_R$. Gostota pade na $0$ šele ko zunanjemu polju obrnemo smer, t.j. pri jakost koercivnega polja $-H_C$.}
    \label{fig:zanka}
\end{figure}

\section{Naloga}

\begin{enumerate}
    \item Izmeri histerezno zanko transformatorskega jekla.
    \item Določi vrednosti gostote magnetnega polja v reži magnetnega kroga sestavljenega iz transformatorskega jekla, kot funkcijo debeline reže, in toka napajanja. Primerjaj izmerjeni rezultat $B_{\mathrm{reža}}$ pri $I = 0$ z vrednostjo, ki jo določiš iz prej izmerjene histerezne krivulje.
    \item Izmeri histerezno krivuljo za magnetni krog sestavljen iz dveh delov. Prvi del je transformatorsko jeklo, drugi del je masivni kos železa. Dodatno: Izračunaj histerezno krivuljo kosa masivnega železa.
\end{enumerate}

\section{Meritve in račun}

Magnetni krog sestavljajajo 4 debele kvadraste stranice z enakim presekom, kvadratom s stranico

\begin{equation*}
    a = \SI{4}{cm} \pm \SI{0.5}{mm}.
\end{equation*}

V sredini vzdolž stranic si namislimo nekakšno osrednjo črto kroga, zaključen kvadrat, ki ima stranici

\begin{align*}
    b &= \SI{10.9}{cm} \pm \SI{0.5}{mm},\\
    c &= \SI{13}{cm} \pm \SI{1}{mm}.
\end{align*}

Prižgemo škatlo s procesnim vezjem in v programu povežemo \verb|COM4|. Magnetni krog sklenemo s kosom transformatorskega jekla in privijemo. Program za zajem umerimo in demagnetiziramo krog. Na vezje naložimo signal sinusne oblike in s privzetimi nastavitvami ($\SI{2.25}{s}$ in 1000 točk) zajamemo meritve.


\begin{figure}
    \centering
    \includegraphics{transformer-loop.pdf}
    \caption{Magnetizacijska krivulja transformatorskega jekla. Na njej vidimo, da je krog sprva nekoliko namagneten, nato pa se magnetizira in "ujame" v histerezno zanko.}
    \label{fig:neprekinjen-krog}
\end{figure}

Zdaj narišemo magnetizacijsko krivuljo kroga iz transformatorskega jekla. Na vodoravni osi prikažemo magnetno jakost $H = \frac{NI}{L}$, pri čemer je $N = 1000$ število navojev primarne tuljave, $L$ pa notranji obseg kroga $L = 2b + 2c$. Na navpični osi pokažemo magnetno gostoto, ki jo iz zajetega integrala $F$ dobimo kot

\begin{equation*}
    B = \frac{F}{Sn} - B_0,
\end{equation*}

kjer je presek kroga $S = a^2$, število ovojev sekundarne tuljave $n = 46$ in konstanta $B_0 = \frac{1}{2} (\mathop\mathrm{max}B - \mathop\mathrm{min} B)$. Ta premik, ki nam zagotovi simetričnost, lahko storimo tudi za drugo os -- a slika je v tej smeri v okviru napake že pravilno poravnana. Končna krivulja je na sliki \ref{fig:neprekinjen-krog}.

Zdaj magnetni krog prekinemo z koščkom papirja, krog demagnetiziramo in meritev ponovimo z istimi nastavitvami. Enako ponovimo z 2, 3, 4, 8 in 16 papirčki. Magnetizacijske krivulje narišemo enako kot za prvo meritev, le da namesto jakosti $H$ na vodoravni osi uporabimo magnetno napetost $U_m = NI$, saj dejanske jakosti $H$ ne poznamo. Krivulje za vse različne debeline rež so prikazane na sliki \ref{fig:reze-krogi}.

Izmerimo skupno debelino 18 papirčkov in izračunamo, da je povprečna debelina posameznega papirčka

\begin{equation*}
    d_0 = \SI{0.09}{mm} \pm \SI{0.6}{\micro m}.
\end{equation*}

Remanentne gostote v krogu z režo lahko zdaj dobimo preprosto kot gostote v presečiščih krivulj z premico $U_m = 0$. Po drugem premisleku iz navodil pa lahko te iste remanentne gostote pri $I = 0$ izračunamo kot presečišča magnetizacijske krivulje transformatorskega jekla s premicami

\begin{equation*}
    B = -\frac{\mu_0 L}{d} H,
\end{equation*}


kjer je $\mu_0$ indukcijska konstanta, $L$ notranji obseg kroga in $d$ debelina lističa, ki je $d = n d_0$ za $n$ lističev. Izračunane in izmerjene remanentne gostote in odstopanja med njimi so prikazane v tabeli \ref{tab:primerjava}.

\begin{figure}
    \centering
    \includegraphics{intersections.pdf}
    \caption{Presečišča (rdeče točke) magnetizacijske krivulje (v modrem) in premic za $I = 0$, prikazanih črtkano od najhitreje padajoče pri $d = d_0$ do najpočasneje padajoče pri $d = 16d_0$.}
    \label{fig:presecisca}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{c|r r |r r}
        $d/d_0$ & $B_R^{\mathrm{rač}}\,[\si{mT}]$ & $B_R^{\mathrm{mer}}\,[\si{mT}]$ & $\Delta B_R\,[\si{mT}]$ & $\delta B_R$\\
        \hline
        1 & 234.7 & 268.4 & 33.7 & 0.1 \\
        2 & 164.8 & 173.0 & 8.2 & 0.05 \\
        3 & 126.9 & 132.3 & 5.4 & 0.04 \\
        4 & 103.2 & 99.5 & 3.7 & 0.04 \\
        8 & 59.1 & 66.2 & 7.1 & 0.1 \\
        16 & 31.9 & 32.1 & 0.2 & 0.007 \\
    \end{tabular}
    \caption{Izračunane in izmerjene remanentne gostote pri krogih z režami različnih debelin. $\Delta B_R$ je absolutno, $\delta B_R$ pa relativno odstopanje med izračunanim $B_R^\mathrm{rač}$ in izmerjenim $B_R^\mathrm{mer}$.}
    \label{tab:primerjava}
\end{table}

\begin{figure}
    \centering
    \includegraphics{loops-side-by-side.pdf}
    \caption{Zanke kroga z režo različnih debelin. Rdeči križci so presečišča z $U_m = 0$ -- izmerjene remanentne gostote kroga.}
    \label{fig:reze-krogi}
\end{figure}

Končno enako meritev kot prej ponovimo še z krogom, v katerem zgornjo prečko kroga iz transformatorskega jekla zamenjamo z enako oblikovano prečko iz železa. Za ta krog prav tako narišemo magnetizacijsko krivuljo, kjer na vodoravni osi spet uporabimo magnetno napetost $U_m$. Krivulja tega kroga je na sliki \ref{fig:delno-železni-krog}.

\begin{figure}
    \centering
    \includegraphics{iron-loop.pdf}
    \caption{Magnetizacijska krivulja kroga, katerega zgornji del zamenjamo z železom.}
    \label{fig:delno-železni-krog}
\end{figure}

Končno lahko izračunamo še magnetizacijsko krivuljo železa. Pogledamo zanji krog, ki ga sestavljata transformatorsko jeklo in železo. Železni del kroga ima dolžino $\ell = b + a$, torej ima del iz transformatorskega jekla dolžino $L - \ell$. Po Amperovem zakonu velja

\begin{equation*}
    H_\mathrm{Fe} \ell + H_j(L - \ell) = NI,
\end{equation*}

iz česar lahko izrazimo vrednost, ki jo želimo izračunati

\begin{equation*}
    H_\mathrm{Fe} = \frac{1}{\ell} \Big[ NI - (L - \ell) H_j \Big].
\end{equation*}

Iz meritve delno železnega kroga poznamo v točki gostoto v železu $B$ in tok skozi primartno tuljavo $I$. Ker je poleg tega magnetna gostota v jeklu in v železu enaka (krog ima konstanten presek), lahko iz meritve neprekinjenega kroga iz transformatorskega jekla za določeno gostoto $B$ (v železu in jeklu) določimo magnetno jakost v jeklu $H_j$.

\end{document}
