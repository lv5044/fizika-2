% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\numberwithin{equation}{subsection}
\setlength{\skip\footins}{1.5cm}

\title{Michelsonov interferometer}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

V eksperimentalni fiziki je pogosto potrebno natančno meriti razdalje. Interferometrija je družina tehnik, katere natančnost zasenči tudi najnatančnejše mehanske načine merjenje dolžin. Običajno pri meritvi opazujemo interferenco med valovoma, ki iz istega izvira potujeta po dveh ločenih poteh. Ko opazimo spremembo v interfernčnem vzorcu, lahko natančno izračunamo, kako se je spremenila razlika med dolžinama poti.

Pogosta izvedba interferometra je t. i. Michelsonov interferometer. Interferometer (slika~\ref{fig:skica}) je sestavljen iz dveh krakov, na koncu katerih sta postavljeni zrcali $Z_1$ in $Z_2$. Zrcalo $Z_2$ ima vijake, s katerimi prilagodimo naklon ogledala. Zrcalo $Z_1$ premikamo nazaj in naprej z mikrometerskim vijakom $M$. V prizmo $P$, postavljeno na stičišču krakov, skozi difuzno steklo $D$ sveti luč $L$. V prizmi se razcepi tako, da del žarka potuje po prvem kraku do zrcala $Z_1$, del žarka pa $Z_2$. Od zrcal se odbije nazaj v prizmo, kjer se del odbije v smer $O$, kjer opazujemo interferenčni vzorec -- projeciran na steno ali neposredno.

\begin{figure}
    \centering
    \import{}{interferometer.pdf_tex}
    \caption{Skica Michelsonovega interferometra, ki ga uporabljamo.}
    \label{fig:skica}
\end{figure}

\section{Naloga}

\begin{enumerate}
    \item Z laserjem naravnaj interferometer ter umeri pomik zrcala $Z_1$ v odvisnosti od
        nastavitve mikrometrskega vijaka.
    \item Izmeri lomni količnik zraka v odvisnosti od zračnega tlaka.
    \item Poišči ekvidistančno lego interferometra.
    \item Izmeri koherenčno dolžino bele svetlobe iz žarnice na volframsko žarilno nitko.
    \item Izmeri valovni dolžini Na dubleta.
\end{enumerate}

\section{Meritve in račun}

\subsection{Delilno razmerje}

He-Ne laser valovne dolžine

\begin{equation*}
    \lambda = (632.8 \pm 0.1)\,\si{nm}
\end{equation*}

usmerimo tako, da sveti skozi prizmo v obe zrcali. Na steni v smeri $O$ vidimo na steni dve piki, ki jih s prilagajanjem vijakov na zrcalu $Z_2$ prekrijemo. Tako nastali interferenčni vzorec je bolj očiten, če pred laser dodamo še difuzno steklo. Vzorec je v obliki popačenih koncentričnih krogov (slika \ref{fig:vzorec}).

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{pattern.jpg}
    \caption{Interferenčni vzorec na steni. Krogi so popačeni zaradi nepravilnosti v prizmi in površinah zrcal.}
    \label{fig:vzorec}
\end{figure}

Zdaj izvedemo meritev. Ko en od partnerjev počasi vrti milimeterski vijak $M$, drugi opazuje prehode interferenčnih prog na steni. Preštejemo

\begin{equation*}
    m = 50 \pm 3
\end{equation*}

prehodov\footnote{Prehod proge je tu mišljen kot premik ene svetle proge na mesto druge svetle proge. Ker sva z laboratorjskim partnerjem prehod razumela kot premik svetle proge na mesto temne proge, sva namesto 100 prehodov, kot je to določeno v navodilu naloge, preštela 50 prehodov.} in ob tem zapišemo začetni položaj vijaka $d_0$ in končni položaj vijaka $d_k$. Meritev ponovimo trikrat (tabela \ref{tab:razmerje}). Ker nas zares zanimajo premiki, ne absolutni položaji vijaka, mislim, da je smiselno, da za napako premikov $d$ vzamemo kar natančnost mikrometerskega vijaka, torej

\begin{equation*}
    \Delta d = \SI{0.01}{mm}
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{c|r r |r}
        meritev & $d_0\,[\si{mm}]$ & $d_k\,[\si{mm}]$ & $d\,[\si{mm}]$ \\
        \hline
        1 & 10.79 & 10.88 & 0.09 \\
        2 & 5.40 & 5.49 & 0.09 \\
        3 & 10.00 & 10.08 & 0.08 \\
    \end{tabular}
    \caption{Meritve položajov vijaka ob prehodu 50 prog interferenčnega vzorca.}
    \label{tab:razmerje}
\end{table}

Razdalja, za katero, se je premaknilo zrcalo, je $\frac{m\lambda}{2}$, pot mikrometerskega vijaka pa $d$. Delilno razmerje je torej

\begin{equation*}
    r = \frac{2d}{m\lambda},
\end{equation*}

Napako $\Delta r$ lahko izračunamo kot

\begin{equation*}
    (\Delta r)^2 = (\mathop{\partial_d} r \cdot \Delta d)^2 + (\mathop{\partial_\lambda} r \cdot \Delta\lambda)^2 + (\mathop{\partial_m} r \cdot \Delta m )^2,
\end{equation*}

pri čemer so parcialni odvodi

\begin{align*}
    \mathop{\partial_d} r &= \frac{2}{m\lambda},\\
    \mathop{\partial_\lambda} r &= \frac{2d}{m \lambda^2},\\
    \mathop{\partial_m} r &= \frac{2d}{m^2 \lambda}.\\
\end{align*}

Delilno razmerje je torej

\begin{equation*}
    r = 5.5 \pm 0.7 = 5.5\,(1 \pm 0.13).
\end{equation*}

\subsection{Odvisnost lomnega količnika od tlaka}

V krak z zrcalom $Z_1$ vstavimo komoro dolžine

\begin{equation*}
    \ell = (50 \pm 1)\,\si{mm}
\end{equation*}

in s tlačilko z barometrom tlak v komori povišamo za $p_1 = \SI{2.4}{bar}$ nad okoljskim. Nato z ventilom iz komore spuščamo zrak in v razmikih $\SI{0.2}{bar}$ zapisujemo padec tlaka $\Delta p$ in število prehodov prog $m$.

Splošno je hitrost svetlobe preprost $\frac{c_0}{n}$, kjer je $c_0$ hitrost svetlobe v vakumu in $n$ indeks refrakcije medija, po katerem svetloba potuje. V našem primeri je torej razlika časov potovanje skozi komoro dolžine $\ell$ pri začetnem tlaku $p_1$, in komoro po padcu tlaka $\Delta p$ preprosto

\begin{equation*}
    \Delta t = \frac{\ell \Delta n}{c_0},
\end{equation*}

kjer je $\Delta n$ sprememba (padec) indeksa refrakcije pri spremembi tlaka $\Delta n$. Ker žarek skozi komoro potuje 2-krat in ker je $\omega = \frac{2\pi c_0}{\lambda}$, velja

\begin{equation*}
     \Delta n = -\frac{m\lambda}{2\ell}.
\end{equation*}

Padce $\Delta n$ pri padcih $\Delta p$ (tabela \ref{tab:p-fit}) lahko povežemo s premico, tako da je $\Delta n = k \Delta p$ (slika \ref{fig:p-fit}). Dobimo premico z naklonom

\begin{equation*}
    k = (0.317 \pm 0.002)\,\si{kbar^{-1}}.
\end{equation*}

Če linearna odvisnost velja tudi drugje, lahko iz tega, da je $p = 0$ pravzaprav vakum, sklepamo, da je lomni količnik  preprosto

\begin{equation*}
    n(p) = kp + 1,
\end{equation*}

pri čemer je $p$ absolutni tlak zraka. Pri tlaku $\SI{1000}{bar}$ je količnik torej preprosto $0.317 \pm 0.002$.

\begin{table}
    \centering
    \begin{tabular}{c|r r}
        $\Delta p\,[\si{bar}]$ & $2m$ & $\Delta n\,[10^{-3}]$ \\
        \hline
        -0.0 & 0 & 0.00 \\
        -0.2 & 20 & -0.25 \\
        -0.2 & 21 & -0.27 \\

        -0.2 & 19 & -0.24 \\
        -0.4 & 41 & -0.52 \\
        -0.4 & 40 & -0.51 \\
        -0.4 & 40 & -0.51 \\
        -0.6 & 61 & -0.77 \\
        -0.6 & 59 & -0.75 \\
        -0.6 & 60 & -0.76 \\
        -0.8 & 82 & -1.04 \\
        -0.8 & 79 & -1.00 \\
        -0.8 & 83 & -1.05 \\
        -1.0 & 103 & -1.30 \\
        -1.0 & 104 & -1.32 \\
        -1.0 & 100 & -1.27 \\
        -1.2 & 123 & -1.56 \\
        -1.2 & 124 & -1.57 \\
        -1.2 & 121 & -1.53 \\
        -1.4 & 146 & -1.85 \\
        -1.4 & 143 & -1.81 \\
        -1.4 & 144 & -1.82 \\
        -1.6 & 163 & -2.06 \\
        -1.6 & 163 & -2.06 \\
        -1.6 & 162 & -2.05 \\
        -1.8 & 178 & -2.25 \\
        -2.0 & 199 & -2.52 \\
        -2.2 & 215 & -2.72 \\
    \end{tabular}
    \caption{Meritve položajov vijaka ob prehodu 50 prog interferenčnega vzorca. Stolpec s številom prog vsebuje preštete prehode svetlih lis v temne, torej 2-kratnik števila prehodov prehodov $m$.}
    \label{tab:p-fit}
\end{table}

\begin{figure}
    \centering
    \includegraphics{pressure-fit.pdf}
    \caption{Meritve padca lomnega količnika $\Delta n \le 0$ pri padcih tlaka $\Delta p \le 0$ in prilagojena premica z naklonom $k$, ki ga iščemo.}
    \label{fig:p-fit}
\end{figure}

\subsection{Ekvidistančna lega in koherenčna dolžina}

Za iskanje ekvidistančne lege laser zamenjamo za živosrebrno žarnico, ki ima v primerjavi z laserjem krajšo koherenčno dolžino. Zdaj v izhod $O$ gledamo direktno. Poiščemo lego, kjer ne vidimo veš mnogo interferenčnih krogov, temveč le nekaj velikih, zaradi nepravilnosti v zrcalu popačenih prog. Tedaj živosrebrno žarnico izklopimo in vklopimo žarnico na žarečo volframsko nitko, ki ima spet krajšo koherenčno dolžino. Vijak previdno vrtimo in najdemo točko, v kateri ima interferenčni vzorec novega svetila največji kontrast. To je ekvidistančna lega -- zapišemo položaj mikrometerskega vijaka

\begin{equation*}
    d_e = (6.66 \pm 0.01)\,\si{mm}.
\end{equation*}

Še vedno v ekvidistančni legi, zrcalo $Z_2$ nagnemo tako, da vidimo okoli 10 prog. Nato zrcalo z mikrometerskim vijakom premikamo do lege, kjer kontrast pade za približno polovico in med tem štejemo proge. Preštejemo

\begin{equation*}
   m = 12 \pm 4
\end{equation*}

prog, iz česar lahko, ker poznamo povprečno valovno dolžino svetila $\overline \lambda = \SI{550}{nm}$, približno izračunamo povprečno koherenčno dolžino

\begin{equation*}
    d_c = (6.6 \pm 2.2)\,\si{nm}
\end{equation*}

oz. koherenčni čas

\begin{equation*}
    \tau_c = (2.2 \pm 0.7) \cdot 10^{-14}\,\si{s}.
\end{equation*}

\begin{table}
    \centering
    \begin{tabular}{c|r r |r |r r |r}
        meritev & $d_0\,[\si{mm}]$ & $d_k\,[\si{mm}]$ & $d\,[\si{mm}]$ & $D_0\,[\si{mm}]$ & $D_k\,[\si{mm}]$ & $D\,[\si{mm}]$\\
        \hline
        1 & 5.00 & 4.93 & 0.07 & 0.43 & 11.60 & 11.17 \\
        2 & 5.00 & 4.93 & 0.07 & 0.43 & 14.64 & 14.21 \\
        3 & 5.00 & 4.92 & 0.08 & 0.43 & 16.42 & 15.99 \\
    \end{tabular}
    \caption{Meritve položajov vijaka in premik $d$ ob prehodu 50 prog interferenčnega vzorca in premik $D$ ob utripanju po 9 minumumih kontrasta.}
    \label{tab:doublet}
\end{table}

\subsection{Natrijev dublet}

Končno si ogledamo še obnašanje Natrijeve svetilke. Prvo premikanje prog, ki ga bi pričakovali v vsakem primeru -- drugo pa utripanje, spremembe v kontrastu z daljšo periodo, ki ga povzročita črti v spektru Natrija, ki sta zelo blizu skupaj. Izmerimo dolžino ob začetku (tabela~\ref{tab:doublet}), pri čemer preštejemo

\begin{equation*}
    m = 50 \pm 3
\end{equation*}

prehodov. Za napaki $d$ in $D$ enako ko prej vzamemo $\SI{0.01}{mm}$ Ker od prej poznamo delilno razmerje $r$, lahko povprečno valovno dolžino $\overline\lambda$ izračunamo kot

\begin{equation*}
    \overline\lambda = \frac{2d}{mr},
\end{equation*}

Enako kot prej za napako poračunamo parcialne odvode

\begin{align*}
    \mathop{\partial_d} \overline\lambda &= \frac{2}{mr},\\
    \mathop{\partial_m} \overline\lambda &= -\frac{2d}{m^2 r},\\
    \mathop{\partial_r} \overline\lambda &= -\frac{2d}{m r^2},\\
\end{align*}

in dobimo rezultat

\begin{equation*}
    \overline\lambda = (535 \pm 109)\,\si{nm}.
\end{equation*}

Za utripanja z daljšo periodo lahko pogoj za prehod 9 minimumov kontrasta razpišemo kot

\begin{align*}
    (k_1 - k_2) \frac{D}{r} &= 9\pi \\
    \frac{2\pi}{\lambda_1} - \frac{2\pi}{\lambda_2} &= \frac{9r\pi}{D} \\
    \frac{\Delta\lambda}{\lambda_1 \lambda_2} &= \frac{9r}{2D}.
\end{align*}

Ker sta $\lambda_1$ in $\lambda_2$ zelo blizu, je torej $\lambda_1\lambda_2 \approx \overline\lambda^2$ in je razlika valovnih dolžin kar

\begin{equation*}
    \Delta\lambda = \frac{9\overline\lambda^2 r}{2D},
\end{equation*}

kar nam za napake da parcialne odvode

\begin{align*}
    \mathop{\partial_{\overline\lambda}} \Delta\lambda &= \frac{9\overline\lambda r}{D},\\
    \mathop{\partial_r} \Delta\lambda &= \frac{9 \overline\lambda^2}{2D},\\
    \mathop{\partial_D} \Delta\lambda &= -\frac{9 \overline\lambda^2 r}{2D^2},\\
\end{align*}

in rezultat

\begin{equation*}
    \Delta\lambda = (0.5 \pm 0.2)\,\si{nm}.
\end{equation*}

Povprečna $\overline\lambda$ in razlika $\Delta\lambda$ se znotraj okvira napake ujemata z vrednostmi, ki sem jih preveril na spletu.

\end{document}
