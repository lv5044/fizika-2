% !TEX program = xelatex

% Base
\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{/home/martin/literatura.bib}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Imported pdf_tex figures
\usepackage{graphicx,import}
\usepackage{color}

% Hyperlinks
\usepackage{hyperref}
\usepackage{xcolor}

% Styling
\setlength{\skip\footins}{1.5cm}


% Differential
\newcommand{\diff}{\mathrm{d}}

% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}

\title{Upogib}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Uvod}

Kvantitativno lokalne deformacije z napetostjo $\sigma = \frac{F}{S}$ povezuje prožnostni modul $E$, tako da velja

\begin{equation*}
    \sigma = E \varepsilon,
\end{equation*}

pri čemer je $\varepsilon = \frac{\Delta l}{l}$ relativi raztezek oz. skrček. Ta linearna zveza velja v t. i. območju elastičnosti, kjer je relativni raztezek še dovolj majhen.

Za preučevanje prožnih deformacij materiala palico na dveh koncih podpremo, na sredi med točkama podpore pa nanjo delujemo s silo uteži. Spodnja stran palice čuti natezno silo in se podaljša, zgornja stran pa se nasprotno ob tlačnih silah skrči. Problem v prvem približku obravnavamo tako, kot da je njen presek vseskozi konstanten.

\begin{figure}
    \centering
    \import{}{skica.pdf_tex}
    \caption{Skica palice s z označenimi dimenzijami in ravninami.}
    \label{fig:skica}
\end{figure}

Palico zdaj po dolžini prerežemo z navidezno ravnino. Material ob tej ravnini je t. i. nevtralna plast, ki se ob obremenitvi le ukrivi, a ohrani začetno dolžino (glej skico~\ref{fig:skica}). Lokalno lahko ukrivljeno palico opišemo kot krožnico, iz česar nato po sorazmernosti radijev $R$ in $R + y'$ ter dolžine loka $l$ in $l + \Delta l$ zapišemo

\begin{equation}
    \varepsilon = \frac{\Delta l}{l} = \frac{y'}{R}.
    \label{eq:ratio}
\end{equation}

Posledica deformacije $\Delta l$ je vzdolžna napetost

\begin{equation*}
    \sigma = E \frac{y'}{R}.
\end{equation*}

Vsota vseh navorov v eno stran, ki plast poskuša vrteti okoli osi $z'$, je

\begin{equation*}
    M = \int_\Omega \sigma y' \,\diff S = \frac{EJ}{R}, \qquad J \das \int_\Omega y'^2 \,\diff S.
\end{equation*}

S preprosto integracijo dobimo $J = a^4/12$ za kvadratni profil s stranico $a$ ter $J = \pi r^4/4$ za okrogel profil z radijem $r$.

Lokalno lahko za ukrivljenost uporabimo

\begin{equation*}
    \frac{1}{R} = u''(x)
\end{equation*}

in navor se poenostavi v

\begin{equation}
    M(x) = EJ u''(x).
    \label{eq:M}
\end{equation}

Za profil palice zdaj potrebujemo le še odvisnost $M(x)$. Če palico razdelimo po ploskvici $\omega$ in gledamo vsoto vseh navorov $M$ na levi kosu palice. Če na palico v koordinatnem sistemu nevtralne ploskve deluje porazdelitev sile $f(x')$, je zunanji $M$ enak

\begin{equation}
    M(x) = \int_0^x f(x') |x - x'| \,\diff x'.
    \label{eq:left}
\end{equation}

Ker upognjena palica miruje in se ne vrti, je ta navor enak vsoti vseh navorov v eno stran na plast $\Omega$, torej navoru v enačbi~(\ref{eq:M}). Prvi odvod integrala~(\ref{eq:left}) je

\begin{align*}
    M'(x) &= (x - x) \cdot 1 - 0 + \int_0^x f(x') \,\diff x' \\
          &= \int_x^0 f(x') \,\diff x' = F(x),
    \label{eq:F}
\end{align*}

iz česar potem sledi osnovna enačba palice

\begin{equation}
    M''(x) = f(x) = EJ u^{(4)}(x).
\end{equation}

Ta enačba nam s centralno obremenitvijo da polinomsko rešitev oblike

\begin{equation*}
    u(x) = a + bx + cx^2 + dx^3.
\end{equation*}

Če zdaj koordinatni sistem prestavimo na sredino palice, so začetni pogoji

\begin{itemize}
    \item $u\left( \frac{l}{2} \right) = 0$, ker je palica vpeta,
    \item $u''\left( \frac{l}{2} \right) = 0$, preko~(\ref{eq:M}) ker je palica gibko vpeta,
    \item $u'''(0) = \frac{F_0}{EJ}$ sila je porazdeljena kot $f(x) = -F_0 \delta(x)$, torej imamo pri $x = 0$ skok $u'''$ velikosti $-\frac{F_0}{EJ}$.
\end{itemize}

kar nam da rešitev

\begin{equation*}
    u(x) = -\frac{F_0 l^3}{48EJ} \left\{ 1 - 6\left( \frac{x}{l} \right)^2 + 4\left( \frac{x}{l} \right)^3 \right\}.
\end{equation*}

Na sredini pri $x = 0$ se palica torej zniža za

\begin{equation}
    u_0 = \frac{F_0 l^3}{48EJ}.
    \label{eq:fin}
\end{equation}

Zanima nas še maksimalna obremenitev, da lokalne deformacije $\varepsilon$ ostanejo pod $0.1\%$. Gledamo najožji radij, ki še dovoljuje, da na celotni višini palice -- torej za $y' \in [0, a]$ ali $y' \in [0, 2r]$ -- deformacija $\varepsilon$ v~(\ref{eq:ratio}) še ostane manjši od $0.1\%$. To nam da preko~\ref{eq:M} maksimalno obremenitev

\begin{equation}
    F_\mathrm{max} \approx \frac{8EJ \varepsilon}{D l},
    \label{eq:max}
\end{equation}

pri čemer je $\varepsilon \equiv 0.001$.

\section{Naloga}

\begin{enumerate}
    \item Opazuj upogibanje dveh palic različnih presekov v odvisnosti od obremenitve in izračunaj njuna prožnostna modula.
    \item Oceni maksimalno obremenitev palic ter za koliko se palici upogneta zaradi lastne teže. Primerjaj tudi gostoti obeh palic.
    \item Nariši diagrama spreminjanja strižne sile in navora vzdolž palice za izbrano utež.
\end{enumerate}

\section{Meritve in račun}

Prvi izmerimo koeficient vzmeti v mikrometru. Postavimo ga na tehtnico in pritisnemo navzdol, nato pa pri različnih izmerjenih odmikih zapišemo maso, ki jo kaže tehtnica. Ker ima tudi igla neko maso, je masa, ki jo kaže tehtnica, enaka

\begin{equation*}
    m' = \frac{k_i \Delta x}{g} + m_i,
\end{equation*}

\begin{figure}
    \centering
    \includegraphics{needle.pdf}
    \caption{}
    \label{fig:needle}
\end{figure}

pri čemer je $k_i$ iskani koeficient vzmeti, $m_i$ masa igle, $g$ gravitacijski pospešek. S preprostim linearnim fitom dobimo za koeficient

\begin{equation*}
    k_i = (49 \pm 6)\,\si{N/m}.
\end{equation*}

Zdaj stehtamo prvo palico ter pomerimo njeno dolžino ter stranico kvadratnega profila

\begin{align*}
    a = (7.00 \pm 0.02)\,\si{mm}, \\
    \ell = (56.00 \pm 0.02)\,\si{cm}, \\
    m_k = (267 \pm 2)\,\si{mm}.
\end{align*}

Iz tega lahko izračunamo moment $J_k$, ki je

\begin{equation*}
    J_k = (2.00 \pm 0.02) \cdot 10^{-10}\,\si{m^4}.
\end{equation*}

Če zdaj izmerimo poves $u_0$ pri različnih utežeh $m$, torej pri poznanih silah

\begin{equation*}
    F_0(u_0) = gm + k_i u_0
\end{equation*}

lahko skozi meritve prilagodimo premico (slika~\ref{fig:profiles} zgoraj) in po formuli~(\ref{eq:fin}) izračunamo prožnostni modul materiala palice

\begin{equation*}
    E_k = (107 \pm 1)\,\si{GPa}.
\end{equation*}

Iz tega po~(\ref{eq:max}) ocenimo maksimalno obtežitev palice

\begin{equation*}
    F_\mathrm{max}^k \approx (44 \pm 1)\,\si{N}.
\end{equation*}

Meritve ponovimo za palico enake dolžine z okroglim profilom premera $d$ in mase $m_\circ$

\begin{equation*}
    d = (7.00 \pm 0.02)\,\si{mm}, \\
    m_\circ = (214 \pm 2)\,\si{g}.
\end{equation*}

Izračunamo, da je njen moment

\begin{equation*}
    J_\circ = (1.18 \pm 0.01) \cdot 10^{-10}\,\si{m^4},
\end{equation*}

njen prožnostni modul in maksimalno obremenitev (meritve in premica na sliki~\ref{fig:profiles} spodaj) pa

\begin{align*}
    E_\circ &= (111 \pm 1)\,\si{GPa}, \\
    F_\mathrm{max}^\circ &\approx (26.7 \pm 0.4)\,\si{N}.
\end{align*}

Hitro še ocenimo, koliko se palici povesita zaradi lastne teže. Preprosto vzamemo, kot da teža prijemlje le v težišču. Za kvadraten profil dobimo

\begin{equation*}
    u_g^k = (0.446 \pm 0.05)\,\si{mm},
\end{equation*}

za okrogel pa

\begin{equation*}
    u_g^\circ = (0.587 \pm 0.07)\,\si{mm}.
\end{equation*}

Na podlagi izjemno podobnih prožnostnih modulov sumimo, da sta palici iz istega materiala. Zato izračunamo še gostoti

\begin{equation*}
    \rho_k = (9730 \pm 90)\,\si{kg/m^3}, \\
    \rho_\circ = (9900 \pm 100)\,\si{kg/m^3}.
\end{equation*}

Gostoti se znotraj napake dobro ujemata, torej zaključimo, da sta palici narejeni iz istega oz. podobnega materiala, konkretno neke vrste medenine.

\begin{figure}
    \centering
    \includegraphics{square.pdf}
    \includegraphics{round.pdf}
    \caption{Meritve odmikov pri silah in prilagajoči premici.}
    \label{fig:profiles}
\end{figure}

Za konec lahko narišemo še fukciji $F(x)$ in $M(x)$ po nevtralni ravnini. Za poljubno točkasto silo v središču narišemo sliko~\ref{fig:F-and-M}.

\begin{figure}
    \centering
    \includegraphics{F-and-M.pdf}
    \caption{Odvisnost strižne sile $F$ in navora $M$ od koordinate $x$ v nevtralni ravnini. Neprekinjena črte je strižna sila $F$, črtkana pa navor $M$.}
    \label{fig:F-and-M}
\end{figure}

\end{document}
